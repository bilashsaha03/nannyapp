import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import LoginScreen from './Screens/LoginScreen';
import WelcomeScreen from './Screens/WelcomeScreen';
import ZeroScreen from './Screens/ZeroScreen';
import NannySignupScreen from './Screens/NannySignupScreen';
import ParentSignupScreen from './Screens/ParentSignupScreen';
import NannyDrawerScreen from './Screens/Nanny/NannyDrawerScreen';
import ParentDrawerScreen from './Screens/Parent/ParentDrawerScreen';
import NavigationService from './NavigationService';

export default class App extends Component {
  async saveToStore(key, value) {
    await AsyncStorage.setItem(key, value);
  }

  render() {
    console.disableYellowBox = true;
    console.reportErrorsAsExceptions = false;
    return (
      <MyApp
        ref={navigatorRef => {
          NavigationService.setRootLevelNavigator(navigatorRef);
        }}
        screenProps={{ saveToStore: this.saveToStore }}
      />
    );
  }
}

const MyApp = createStackNavigator(
  {
    Login: { screen: props => <LoginScreen {...props} /> },
    Welcome: { screen: props => <WelcomeScreen {...props} /> },
    Zero: { screen: props => <ZeroScreen {...props} /> },
    NannySignup: { screen: props => <NannySignupScreen {...props} /> },
    ParentSignup: { screen: props => <ParentSignupScreen {...props} /> },
    NannyDrawer: { screen: props => <NannyDrawerScreen {...props} /> },
    ParentDrawer: { screen: props => <ParentDrawerScreen {...props} /> }
  },
  {
    initialRouteName: 'Zero',
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    },
  },
);
