// NavigationService.js

import { NavigationActions, StackActions } from 'react-navigation';

let _navigator_nanny;
let _navigator_parent;
let _navigator_root;
const api_url = "http://13.229.125.204/api"; //aws server
//const api_url = "http://192.168.43.60:3000/api"; //bazlur

function setNannyLevelNavigator(navigatorRef) {
  _navigator_nanny = navigatorRef;
}

function setParentLevelNavigator(navigatorRef) {
  _navigator_parent = navigatorRef;
}


function setRootLevelNavigator(navigatorRef) {
  _navigator_root = navigatorRef;
}

function navigate_parent(routeName, params) {
  _navigator_parent.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    })
  );
}

function navigate_nanny(routeName, params) {
  _navigator_nanny.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    })
  );
}

function navigate_root(routeName, params) {
  _navigator_root.dispatch(
      StackActions.reset({
        index: 0,
        key: null,
        actions: [NavigationActions.navigate({ routeName, params})]
    })
  );
}

// add other navigation functions that you need and export them

export default {
  navigate_nanny,
  navigate_root,
  navigate_parent,
  setNannyLevelNavigator,
  setRootLevelNavigator,
  setParentLevelNavigator,
  api_url
};
