import React, { Component } from 'react';
import {
  Text,
  ScrollView,
  AsyncStorage,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {
  Icon,
  Container,
  Header,
  Content,
  Left,
  Body,
  Right,
} from 'native-base';
import NavigationService from '../NavigationService';
import Loader from 'react-native-modal-loader';
import MasterStyles from '../Styles';

export default class ChangeNameScreen extends Component {
  constructor() {
    super();
    this.state = {
      authentication_token: '',
      email: '',
      name: '',
      user: {},
      isLoading: false,
    };
  }

  async getUser() {
    this.setState({ isLoading: true });
    try {
      let response = await fetch(
        `${NavigationService.api_url}/users/${await AsyncStorage.getItem(
          'user_id',
        )}`,
        {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'X-Auth-Token': await AsyncStorage.getItem('authentication_token'),
            'X-Auth-Email': await AsyncStorage.getItem('email'),
          },
        },
      );
      let responseJson = await response.json();
      if (response.ok) {
        this.setState({ user: responseJson.user, isLoading: false });
      } else {
        this.setState({ isLoading: flase });
      }
    } catch (error) {
      console.error(error);
      this.setState({ isLoading: flase });
    }
  }

  componentDidMount() {
    this.getUser();
  }

  async changeName() {
    this.setState({ isLoading: true });
    try {
      let response = await fetch(
        `${NavigationService.api_url}/users/${await AsyncStorage.getItem(
          'user_id',
        )}`,
        {
          method: 'PUT',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'X-Auth-Token': await AsyncStorage.getItem('authentication_token'),
            'X-Auth-Email': await AsyncStorage.getItem('email'),
          },
          body: JSON.stringify({
            user: {
              name: this.state.user.name,
            },
          }),
        },
      );
      let responseJson = await response.json();
      if (response.ok) {
        await AsyncStorage.setItem('name', this.state.user.name);
        this.setState({ isLoading: false });
        if (responseJson.role == 'passenger') {
          NavigationService.navigate_passenger('MyBookings');
        } else if (responseJson.role == 'driver') {
          NavigationService.navigate_driver('DriverStart');
        } else if (responseJson.role == 'super_admin') {
          NavigationService.navigate_root('AdminTrips');
        }
      } else {
        this.setState({ isLoading: false });
      }
    } catch (error) {
      console.error(error);
      this.setState({ isLoading: false });
    }
  }

  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: '#11bde4' }}>
          <Left style={{ paddingTop: 20 }}>
            <Icon
              name="ios-menu"
              style={{ color: '#FFF' }}
              onPress={() => this.props.navigation.openDrawer()}
            />
          </Left>
          <Body style={{ alignItems: 'center' }}>
            <Text style={{ paddingTop: 20, fontWeight: 'bold', color: '#FFF' }}>
              Change Your Name
            </Text>
          </Body>
          <Right style={{}} />
        </Header>
        <Content
          contentContainerStyle={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <ScrollView
            keyboardShouldPersistTaps="handled"
            style={{ padding: 20 }}
          >
            <Loader loading={this.state.isLoading} color="#11bde4" />
            <Text style={{ fontSize: 27 }}>Your Name</Text>

            <TextInput
              value={this.state.user.name}
              style={MasterStyles.textinput}
              onChangeText={value => {
                let user = this.state.user;
                user.name = value;
                this.setState({ user: user });
              }}
            />
            <TouchableOpacity
              style={MasterStyles.button}
              onPress={this.changeName.bind(this)}
            >
              <Text style={MasterStyles.btntext}>Submit</Text>
            </TouchableOpacity>
          </ScrollView>
        </Content>
      </Container>
    );
  }
}
