import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  AsyncStorage,
  Image
} from 'react-native';
import { Container, Content } from 'native-base';
import NavigationService from '../NavigationService';
import Loader from 'react-native-modal-loader';
import { FormLabel, FormInput } from 'react-native-elements'
import MasterStyles from '../Styles';

export default class LoginScreen extends Component {
  constructor() {
    super();
    this.state = {
      user_id: '',
      authentication_token: '',
      email: '',
      password: '',
      warning_message: false,
      isLoading: false,

    };
  }

  componentDidMount() {
    this.checkAlreadyloggedIn();
  }

  async checkAlreadyloggedIn() {
    this.setState({ isLoading: true });
    auth_token = await AsyncStorage.getItem('authentication_token');
    role = await AsyncStorage.getItem('role');
    if (auth_token) {
      this.setState({ isLoading: false });
      if (role == 'nanny') {
        NavigationService.navigate_root('NannyDrawer');
      } else if (role == 'parent') {
        NavigationService.navigate_root('ParentDrawer');
      }
    }
    this.setState({ isLoading: false });
  }

  async login() {
    this.setState({ isLoading: true });
    try {
      let response = await fetch(`${NavigationService.api_url}/login/`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            email: this.state.email,
            password: this.state.password
          }
        ),
      });
      let responseJson = await response.json();
      if (response.ok) {
        this.setState({ isLoading: false, warning_message: false }, () => {
          this.props.screenProps.saveToStore(
            'authentication_token',
            responseJson.user.auth_token,
          );
          this.props.screenProps.saveToStore('email', responseJson.user.email);
          this.props.screenProps.saveToStore('user_id', `${responseJson.user.id}`);
          this.props.screenProps.saveToStore('role', `${responseJson.role}`);
          this.props.screenProps.saveToStore('name', `${responseJson.name}`);
          this.props.screenProps.saveToStore('photo_url', `${responseJson.photo_url}`);
          console.log(responseJson.role)
          if (responseJson.role == 'nanny') {
            NavigationService.navigate_root('NannyDrawer');
          } else if (responseJson.role == 'parent') {
            NavigationService.navigate_root('ParentDrawer');
          }
        });

      } else {
        this.setState({ isLoading: false, warning_message: true });
      }
    } catch (error) {
      this.setState({ isLoading: false });
      alert('Something went wrong, please try again.');
    }
  }

  render() {
    return (
      <Container>
        <Content
          contentContainerStyle={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#11bde4',
          }}
        >
          <Loader loading={this.state.isLoading} color="#11bde4" />

          <Image
            style={MasterStyles.ImageStyle}
            source={require('../assets/logo.png')}
          />
          <Text style={{ fontSize: 26, color: '#FFF', fontWeight: 'bold' }}>
            Welcome to Nanny App
          </Text>
          <FormLabel labelStyle={{color: "#000"}} >Email (ই-মেইল)</FormLabel>
          <FormInput inputStyle={MasterStyles.fromInputText}  onChangeText={(text) => this.setState({email: text})} />

          <FormLabel labelStyle={{color: "#000"}} >Password (পাসওয়ার্ড)</FormLabel>
          <FormInput inputStyle={MasterStyles.fromInputText} secureTextEntry={true}
                     onSubmitEditing={this.login.bind(this)}
                     onChangeText={(text) => this.setState({password: text})} />

          <TouchableOpacity
            style={{
              width: 340,
              alignItems: 'center',
              padding: 20,
              marginTop: 8,
              backgroundColor: '#FFF',
            }}
            onPress={this.login.bind(this)}
          >
            <Text style={{ fontWeight: 'bold', fontSize: 20, color: '#000' }}>
              Sign in
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              width: 340,
              alignItems: 'center',
              padding: 20,
              marginTop: 8,
              backgroundColor: '#FFF',
            }}
            onPress={() => {this.props.navigation.navigate('ParentSignup')}}
          >
            <Text style={{ fontWeight: 'bold', fontSize: 20, color: '#000' }}>
              Sign Up as Parent
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: 340,
              alignItems: 'center',
              padding: 20,
              marginTop: 8,
              backgroundColor: '#FFF',
            }}
            onPress={() => {this.props.navigation.navigate('NannySignup')}}
          >
            <Text style={{ fontWeight: 'bold', fontSize: 20, color: '#000' }}>
              Sign Up as Nanny
            </Text>
          </TouchableOpacity>

          {this.state.warning_message && (
            <View>
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: 'bold',
                  color: '#F00',
                  marginTop: 8,
                }}
              >
                Email or Password is Invalid
              </Text>
            </View>
          )}
        </Content>
      </Container>
    );
  }
}
