import React, { Component } from 'react';
import { ScrollView, Text, TouchableOpacity, AsyncStorage } from 'react-native';
import { Container, Header, Content, Left, Body, Right } from 'native-base';
import NavigationService from '../NavigationService';
import MasterStyles from '../Styles';

export default class LogoutScreen extends Component {
  constructor() {
    super();
  }

  async logout() {
    await AsyncStorage.setItem('authentication_token', '');
    await AsyncStorage.setItem('email', '');
    await AsyncStorage.setItem('user_id', '');
  }

  componentDidMount() {
    this.logout();
  }

  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: '#11bde4' }}>
          <Left style={{ paddingTop: 20 }} />
          <Body style={{ alignItems: 'center' }}>
            <Text style={{ paddingTop: 20, fontWeight: 'bold', color: '#FFF' }}>
              Nanny App
            </Text>
          </Body>
          <Right style={{}} />
        </Header>
        <Content
          contentContainerStyle={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <ScrollView style={{ alignSelf: 'stretch', flex: 1, padding: 4 }}>
            <Text style={{ fontSize: 24, marginTop: 80, marginBottom: 20, textAlign: 'center' }}>Logged Out Successfully</Text>
            <TouchableOpacity
              style={MasterStyles.button}
              onPress={() => NavigationService.navigate_root('Login')}
            >
              <Text style={MasterStyles.btntext}>Login (লগ ইন)</Text>
            </TouchableOpacity>
          </ScrollView>
        </Content>
      </Container>
    );
  }
}
