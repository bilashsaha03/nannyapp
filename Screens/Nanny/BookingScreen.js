import React, { Component } from 'react';
import {
  Text,
  View,
  AsyncStorage,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import { Container, Content } from 'native-base';
import moment from 'moment';
import NavigationService from '../../NavigationService';
import MasterStyles from '../../Styles';
import Loader from 'react-native-modal-loader';

export default class BookingScreen extends Component {
  constructor() {
    super();
    this.state = {
      authentication_token: '',
      booking_id: '',
      booking: {},
      isLoading: false,
    };
  }

  async getBooking(bookingId) {
    this.setState({ isLoading: true });
    try {
      let response = await fetch(
        `${NavigationService.api_url}/bookings/${bookingId}`,
        {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'X-Auth-Token': await AsyncStorage.getItem('authentication_token'),
            'X-Auth-Email': await AsyncStorage.getItem('email'),
          },
        },
      );
      let responseJson = await response.json();
      if (response.status == 200) {
        this.setState({ booking: responseJson.booking, isLoading: false });
      } else {
        this.setState({ isLoading: false });
        alert(`Errors:\n${responseJson.error}`);
      }
    } catch (error) {
      this.setState({ isLoading: false });
      alert('Something went wrong, please try again.');
    }
  }

  componentDidMount() {
    const bookingId = this.props.navigation.getParam('bookingId', '0');
    this.getBooking(bookingId);
  }

  render() {
    return (
      <Container>
        <Content contentContainerStyle={{ flex: 1, alignSelf: 'stretch' }}>
          <ScrollView style={MasterStyles.scrollViewStyle}>
            <Loader loading={this.state.isLoading} color="#11bde4" />
            {this.state.booking.id && (
              <View
                style={{
                  backgroundColor: '#DADADA',
                  margin: 5,
                  padding: 5,
                  borderRadius: 10,
                  borderWidth: 1,
                }}
              >
                <Text style={MasterStyles.labelText}>
                  Date Of Journey :{' '}
                  {moment(this.state.booking.journey_at).format('DD/MM/YYYY')}{' '}
                </Text>
                <Text style={MasterStyles.labelText}>
                  Time Of Journey :{' '}
                  {moment(this.state.booking.journey_at).format('hh:mm a')}{' '}
                </Text>
                <Text style={MasterStyles.labelText}>
                  Pickup Point : {this.state.booking.pickup_point.name}{' '}
                </Text>
                <Text style={MasterStyles.labelText}>
                  Dropoff Point : {this.state.booking.dropoff_point.name}{' '}
                </Text>
              </View>
            )}
            <TouchableOpacity
              style={MasterStyles.button}
              onPress={() => NavigationService.navigate_passenger('MyBookings')}
            >
              <Text style={MasterStyles.btntext}>Done</Text>
            </TouchableOpacity>
          </ScrollView>
        </Content>
      </Container>
    );
  }
}
