import React, { Component } from 'react';
import {
  ScrollView,
  Text,
  View,
  AsyncStorage,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {
  Icon,
  Container,
  Header,
  Content,
  Left,
  Body,
  Right,
} from 'native-base';
import NavigationService from '../../NavigationService';
import MasterStyles from '../../Styles';
import Loader from 'react-native-modal-loader';
import { List, ListItem, Input } from 'react-native-elements';
import {FormInput} from "react-native-elements";

export default class HostsScreen extends Component {
  constructor() {
    super();
    this.state = {
      hosts: [],
      search_key: "",
      isLoading: false,
    };
  }

  async getAllHosts() {
    this.setState({ isLoading: true });
    try {
      let response = await fetch(`${NavigationService.api_url}/hosts`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'enableEmptySections':true,
          'Authorization': 'Token token=' + await AsyncStorage.getItem('authentication_token')
        },
      });
      let responseJson = await response.json();
      if (response.ok) {
        this.setState({ hosts: responseJson.hosts, isLoading: false });
      } else {
        this.setState({ isLoading: false });
        alert(`Errors:\n${responseJson.error}`);
      }
    } catch (error) {
      this.setState({ isLoading: false });
      alert('Something went wrong, please try again.');
    }
  }

  async search() {
    console.log("Searchingg..")
    this.setState({ isLoading: true });
    try {
      let response = await fetch(`${NavigationService.api_url}/hosts?q=${this.state.search_key}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'enableEmptySections':true,
          'Authorization': 'Token token=' + await AsyncStorage.getItem('authentication_token')
        },
      });
      let responseJson = await response.json();
      if (response.ok) {
        this.setState({ hosts: responseJson.hosts, isLoading: false });
      } else {
        this.setState({ isLoading: false });
        alert(`Errors:\n${responseJson.error}`);
      }
    } catch (error) {
      this.setState({ isLoading: false });
      alert('Something went wrong, please try again.');
    }
  }

  componentDidMount() {
    this.getAllHosts();
  }

  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: '#11bde4' }}>
          <Left style={{ paddingTop: 20 }}>
            <Icon
              name="ios-menu"
              style={{ color: '#FFF' }}
              onPress={() => this.props.navigation.openDrawer()}
            />
          </Left>
          <Body>
            <Text style={{ paddingTop: 20, fontWeight: 'bold', color: '#FFF' }}>
              All Hosts
            </Text>
          </Body>
          <Right style={{}} />
        </Header>
        <Content
          contentContainerStyle={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <ScrollView style={{ alignSelf: 'stretch', flex: 1, padding: 4 }}>
            <Loader loading={this.state.isLoading} color="#11bde4" />

            <FormInput inputStyle={MasterStyles.fromInputText}
              placeholder="Search..."
              onChangeText={(text) => this.setState({search_key:text}) }
            />
            <TouchableOpacity
              style={MasterStyles.button}
              onPress={this.search.bind(this)}>
              <Text style={MasterStyles.btntext}>Search</Text>
            </TouchableOpacity>


            <List containerStyle={{ borderTopColor: '#11bde4' }}>
              <FlatList
                data={this.state.hosts}
                renderItem={({ item }) => (
                  <ListItem
                    containerStyle={{ borderBottomColor: '#11bde4' }}
                    titleStyle={{ color: '#0d0b59', paddingLeft: 16, fontSize: 17, fontWeight: 'bold'}}
                    title={`${item.description}`}
                    subtitle={
                      <View style={MasterStyles.subtitleView}>
                        <Text style={MasterStyles.subtitleText}>
                            {' '}
                          Availability :{' '}
                            { item.availability &&
                            'Yes'
                            }
                            { !item.availability &&
                            'No'
                            }
                        </Text>

                        <Text style={MasterStyles.subtitleText}>
                            {' '}
                          Price :{' '}
                            {item.price}{' '}
                        </Text>
                        <Text style={MasterStyles.subtitleText}>
                            {' '}
                          Location :{' '}
                            {item.owned_by.police_station.name}{' '}
                        </Text>
                      </View>
                    }
                    leftIcon={{ name: 'assignment', color: 'blue' }}
                    onPress={() => {
                      this.props.navigation.navigate('HostDetail', {
                        hostId: item.id,
                      })
                    }
                    }
                  />
                )}
                keyExtractor={item => item.name}
              />
            </List>
          </ScrollView>
        </Content>
      </Container>
    );
  }
}
