import React, { Component } from 'react';
import {
  ScrollView,
  Text,
  View,
  AsyncStorage,
  TouchableOpacity,
  FlatList,
    Image
} from 'react-native';
import {
  Icon,
  Container,
  Header,
  Content,
  Left,
  Body,
  Right,
} from 'native-base';
import NavigationService from '../../NavigationService';
import MasterStyles from '../../Styles';
import Loader from 'react-native-modal-loader';
import { List, ListItem, Input } from 'react-native-elements';
import {FormInput, FormLabel} from "react-native-elements";
import moment from 'moment';

export default class JobDetailScreen extends Component {
  constructor() {
    super();
    this.state = {
      job: {
        posted_by : {
          police_station: {}
        }
      },
      isLoading: false,
      job_application: {},
      bid_amount: null
    };
  }

  async getJob(id) {
    this.setState({ isLoading: true });
    try {
      let response = await fetch(`${NavigationService.api_url}/jobs/${id}/`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'enableEmptySections':true,
          'Authorization': 'Token token=' + await AsyncStorage.getItem('authentication_token')
        },
      });
      let responseJson = await response.json();
      if (response.ok) {
        this.setState({ job: responseJson.job, isLoading: false });
      } else {
        this.setState({ isLoading: false });
        alert(`Errors:\n${responseJson.error}`);
      }
    } catch (error) {
      this.setState({ isLoading: false });
      alert('Something went wrong, please try again.');
    }
  }

  componentDidMount() {
    let jobId = this.props.navigation.getParam('jobId', '0')
    this.getJob(jobId);
  }

  async apply(){
    let job_application = this.state.job_application;
    job_application.job_id = this.state.job.id
    job_application.bid_amount = this.state.bid_amount;

    this.setState({ isLoading: true });
    try {
      let response = await fetch(`${NavigationService.api_url}/job_applications`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'enableEmptySections':true,
          'Authorization': 'Token token=' + await AsyncStorage.getItem('authentication_token')
        },
        body: JSON.stringify(job_application
        ),
      });
      let responseJson = await response.json();
      if (response.ok) {
        let job = this.state.job;
        job.applied = true
        this.setState({ isLoading: false, job: job }, () => {

        });

      } else {
        this.setState({ isLoading: false, warning_message: true });
      }
    } catch (error) {
      this.setState({ isLoading: false });
      alert('Something went wrong, please try again.');
    }

  }


  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: '#11bde4' }}>
          <Left style={{ paddingTop: 20 }}>
            <Icon
              name="ios-menu"
              style={{ color: '#FFF' }}
              onPress={() => this.props.navigation.openDrawer()}
            />
          </Left>
          <Body>
            <Text style={{ paddingTop: 20, fontWeight: 'bold', color: '#FFF' }}>
             Job Details
            </Text>
          </Body>
          <Right style={{}} />
        </Header>
        <Content
          contentContainerStyle={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <ScrollView style={{ alignSelf: 'stretch', flex: 1, padding: 10 }}>
            <Loader loading={this.state.isLoading} color="#11bde4" />
            <Text style={MasterStyles.titleTextInside}>Job Information</Text>
            <Text style={MasterStyles.titleText}>
                {this.state.job.title}
              </Text>
              <Text style={MasterStyles.descriptionText}>
                {this.state.job.description}
              </Text>
            <View style={{flexDirection: 'row'}}>
              <Text style={MasterStyles.detailLabel}>Type :</Text>
              <Text style={MasterStyles.detailValue}>{this.state.job.job_type}</Text>
            </View>
              { this.state.job.job_type == 'Recurring' &&
                  <View style={{flexDirection: 'row'}}>
                    <Text style={MasterStyles.detailLabel}>Weekdays :</Text>
                    <Text style={MasterStyles.detailValue}>{this.state.job.weekdays}</Text>
                  </View>
              }
            <View style={{flexDirection: 'row'}}>
              <Text style={MasterStyles.detailLabel}>Duration :</Text>
              <Text style={MasterStyles.detailValue}>{this.state.job.duration} hours</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={MasterStyles.detailLabel}>Budget :</Text>
              <Text style={MasterStyles.detailValue}>{this.state.job.budget}{' Tk'}</Text>
            </View>

            <View style={{flexDirection: 'row'}}>
              <Text style={MasterStyles.detailLabel}>Skills :</Text>
              <Text style={MasterStyles.detailValue}>{ this.state.job.required_skills }</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={MasterStyles.detailLabel}>Services :</Text>
              <Text style={MasterStyles.detailValue}>{ this.state.job.required_services }</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={MasterStyles.detailLabel}>Age Range :</Text>
              <Text style={MasterStyles.detailValue}>{ this.state.job.age_range }</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={MasterStyles.detailLabel}>Created :</Text>
              <Text style={MasterStyles.detailValue}>{this.state.job.created_at}{' ago'}</Text>
            </View>

            <Text style={MasterStyles.titleTextInside}>Parent Information </Text>

            { this.state.job.posted_by && this.state.job.posted_by.photo_url &&
              <View style={{flexDirection: 'row'}}>
                <Text style={MasterStyles.detailLabel}></Text>
                <Text style={MasterStyles.detailValue}><Image source={{uri: this.state.job.posted_by.photo_url}}
                                                              style={MasterStyles.ProfilePhoto}/></Text>
              </View>
            }
            <View style={{flexDirection: 'row'}}>
              <Text style={MasterStyles.detailLabel}>Full Name :</Text>
              <Text style={MasterStyles.detailValue}>{this.state.job.posted_by.name}</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={MasterStyles.detailLabel}>Location (স্থান) :</Text>
              <Text style={MasterStyles.detailValue}>{ this.state.job.posted_by.police_station.name }</Text>
            </View>

            { !this.state.job.applied &&
              <View>
                <FormLabel labelStyle={{color: "#000",fontSize:15, alignSelf: 'center'}}>Enter bid amount(Tk)</FormLabel>
                <FormInput inputStyle={MasterStyles.fromInputText} keyboardType="numeric"
                  onChangeText={(text) => this.setState({bid_amount:text}) }
                  />
              </View>
            }
          </ScrollView>
          { !this.state.job.applied &&
            <TouchableOpacity
              style={MasterStyles.button}
              onPress={this.apply.bind(this)}>
              <Text style={MasterStyles.btntext}>Apply</Text>
            </TouchableOpacity>
          }
          { this.state.job.applied &&
          <Text style={{fontSize: 15,color: "#137617"}}>
            Applied Successfully !!
          </Text>
          }
          { this.state.job.applied &&
          <TouchableOpacity
            style={MasterStyles.button}
            onPress={()=> this.props.navigation.navigate('Jobs')}>
            <Text style={MasterStyles.btntext}>Find jobs</Text>
          </TouchableOpacity>
          }
        </Content>
      </Container>
    );
  }
}
