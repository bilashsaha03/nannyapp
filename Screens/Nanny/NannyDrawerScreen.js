import React, {Component} from 'react';
import {AsyncStorage, ScrollView, Text, StyleSheet} from 'react-native';
import {
    createDrawerNavigator,
    DrawerItems,
    SafeAreaView,
} from 'react-navigation';
import NavigationService from '../../NavigationService';
import LogoutScreen from '../LogoutScreen';
import ProfileScreen from './ProfileScreen';
import {ListItem} from 'react-native-elements';
import HomeScreen from "./HomeScreen";
import JobsScreen from "./JobsScreen";
import HostsScreen from "./HostsScreen";
import JobDetailScreen from "./JobDetailScreen";
import NannyRunningJobsScreen from "./NannyRunningJobsScreen";
import NannyAppliedJobsScreen from "./NannyAppliedJobsScreen";
import NannyCompletedJobsScreen from "./NannyCompletedJobsScreen";
import {
    Icon
} from 'native-base';


class Hidden extends React.Component {
    render() {
        return null;
    }
}

export default class NannyDrawerScreen extends Component {
    constructor() {
        super();
        this.state = {
            name: '...',
            photo_url: '',
        };
    }

    async getName() {
        AsyncStorage.getItem('name').then(value => {
            this.setState({name: value});
        });
        AsyncStorage.getItem('photo_url').then(value => {
            this.setState({photo_url: value});
        });
    }

    componentDidMount() {
        this.getName();
    }

    componentWillReceiveProps() {
        this.getName();
    }

    render() {
        return (
            <NannyStart
                ref={navigatorRef => {
                    NavigationService.setNannyLevelNavigator(navigatorRef);
                }}
                screenProps={{
                    saveToStore: this.saveToStore,
                    name: this.state.name,
                    phone_number: this.state.phone_number,
                    photo_url: this.state.photo_url,
                }}
            />
        );
    }
}

const CustomDrawerContentComponent = props => (
    <ScrollView>
        <SafeAreaView
            style={styles.container}
            forceInset={{top: 'always', horizontal: 'never'}}
        >
            <ListItem
                roundAvatar
                avatar={{uri: props.screenProps.photo_url}}
                key={props.screenProps.name}
                title={props.screenProps.name}
                hideChevron={true}
            />
            <DrawerItems {...props} />
            <Text style={{marginLeft: 15}}>Version: 0.1.0</Text>
        </SafeAreaView>
    </ScrollView>
);

const NannyStart = createDrawerNavigator(
    {
        Home: {
            screen: props => <HomeScreen {...props} />,
            navigationOptions: {
                title: 'Home',
            },
        },
        Profile: {
            screen: props => <ProfileScreen {...props} />,
            navigationOptions: {
                title: 'Update Profile',
            drawerIcon: () => (
                <Icon name='create' style={{color: '#000000'}} />
            )

            },
        },
        Jobs: {
            screen: props => <JobsScreen {...props} />,
            navigationOptions: {
                title: 'Find Jobs',
                drawerIcon: () => (
                    <Icon name='search' style={{color: '#000000'}} />
                )
            },
        },

        AppliedJobs: {
            screen: props => <NannyAppliedJobsScreen {...props} />,
            navigationOptions: {
                title: 'Applied Jobs',
            drawerIcon: () => (
                <Icon name='bookmark' style={{color: '#800040'}} />
            )
            },
        },
        RunningJobs: {
            screen: props => <NannyRunningJobsScreen {...props} />,
            navigationOptions: {
                title: 'Running Jobs',
                drawerIcon: () => (
                    <Icon name='bookmark' style={{color: '#004c00'}} />
                )
            },
        },
        CompletedJobs: {
            screen: props => <NannyCompletedJobsScreen {...props} />,
            navigationOptions: {
                title: 'Completed Jobs',
                drawerIcon: () => (
                    <Icon name='bookmark' style={{color: '#000000'}} />
                )
            },
        },

        JobDetail: {
            screen: props => <JobDetailScreen {...props} />,
            navigationOptions: {
                title: 'Job Detail',
                drawerLabel: <Hidden />
            },
        },
        Hosts: {
            screen: props => <HostsScreen {...props} />,
            navigationOptions: {
                title: 'Hosts',
                drawerIcon: () => (
                    <Icon name='link' style={{color: '#000000'}} />
                )
            },
        },
        Logout: {
            screen: props => <LogoutScreen {...props} />,
            navigationOptions: {
                title: 'Logout',
                drawerIcon: () => (
                    <Icon name='log-out'
                          style={{color: '#990000'}} />
                )
            },
        }
    },
    {
        contentComponent: props => <CustomDrawerContentComponent {...props} />,
        drawerPosition: 'left',
        initialRouteName: 'Profile',
        navigationOptions: {
            headerStyle: {
                backgroundColor: '#11bde4',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
                color: '#FFFFFF',
            },
        },
    },
);

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
