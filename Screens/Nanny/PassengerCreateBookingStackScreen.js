import React, { Component } from 'react';
import { StyleSheet, AsyncStorage } from 'react-native';
import JourneyScreen from './JourneyScreen';
import BookingScreen from './BookingScreen';
import {createStackNavigator} from 'react-navigation';

export default class PassengerCreateBookingStackScreen extends Component {
  async saveToStore(key, value) {
    await AsyncStorage.setItem(key, value);
  }

  render() {
    return <PassengerBooking screenProps={{ saveToStore: this.saveToStore }} />;
  }
}

const PassengerBooking = createStackNavigator(
  {
    Journey: {
      screen: props => <JourneyScreen {...props} />,
      navigationOptions: {
        title: 'Create Booking',
      },
    },
    Booking: {
      screen: props => <BookingScreen {...props} />,
      navigationOptions: {
        title: "Booking Confirmation",
      }
    },
    SelectRoute: {
      screen: props => <SelectRouteScreen {...props}  />,
      navigationOptions: {
        title: "Select Route",
      }
    },
    SelectPickup: {
      screen: props => <SelectPickupScreen {...props}  />,
      navigationOptions: {
        title: "Select Pickup Point",
      }
    },
    SelectDropoff: {
      screen: props => <SelectDropoffScreen {...props}  />,
      navigationOptions: {
        title: "Select Dropoff Point",
      }
    }
  },
  {
    initialRouteName: 'Journey',
    navigationOptions: {
      headerLeft: null,
      headerStyle: {
        backgroundColor: '#11bde4',
      },
      headerTintColor: '#000',
      headerTitleStyle: {
        fontWeight: 'bold',
        fontSize: 20,
        color: '#FFFFFF',
      },
    },
  },
);
