import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {
  Container,
  Header,
  Content,
  Body,
  Left,
  Icon,
  Right,
} from 'native-base';
import NavigationService from '../NavigationService';
import MasterStyles from '../Styles';
import Loader from 'react-native-modal-loader';

export default class OtpConfirmationScreen extends Component {
  constructor() {
    super();
    this.state = {
      phone_number: '',
      otp_sent: false,
      invalid_otp: false,
      otp: '',
      warning_message: false,
      isLoading: false,
    };
  }

  componentDidMount() {
    const phoneNumber = this.props.navigation.getParam('phoneNumber', false);
    this.setState({
      phone_number: phoneNumber,
      isLoading: false,
    });
  }

  async verifyOtp() {
    this.setState({ isLoading: true });
    try {
      let response = await fetch(`${NavigationService.api_url}/otps/`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          phone_number: this.state.phone_number,
          otp_number: this.state.otp,
        }),
      });
      let responseJson = await response.json();
      if (response.ok) {
        const { user } = responseJson;
        this.setState(
          {
            invalid_otp: false,
            otp_sent: false,
            invalid_otp: false,
            otp: '',
            isLoading: false,
          },
          () => {
            this.props.screenProps.saveToStore(
              'authentication_token',
              user.authentication_token,
            );
            this.props.screenProps.saveToStore('email', user.email);
            this.props.screenProps.saveToStore('user_id', `${user.id}`);
            this.props.screenProps.saveToStore('role', `${user.role}`);
            this.props.screenProps.saveToStore('name', `${user.name}`);
            this.props.screenProps.saveToStore(
              'phone_number',
              `${user.phone_number}`,
            );

            if (!user.name) {
              NavigationService.navigate_root('Name');
              return;
            }

            if (user.role == 'passenger') {
              NavigationService.navigate_root('PassengerDrawer');
            } else if (user.role == 'driver') {
              NavigationService.navigate_root('DriverDrawer');
            } else if (user.role == 'super_admin') {
              NavigationService.navigate_root('AdminDrawer');
            }
          },
        );
      } else {
        this.setState({ invalid_otp: true, isLoading: false });
      }
    } catch (error) {
      console.error(error);
      this.setState({ isLoading: false });
    }
  }

  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: '#11bde4' }}>
          <Left style={{ paddingTop: 20 }}>
            <Icon
              name="ios-arrow-back"
              onPress={() => this.props.navigation.navigate('Otp')}
            />
          </Left>
          <Body>
            <Text
              style={{
                alignSelf: 'center',
                paddingTop: 20,
                fontWeight: 'bold',
                fontSize: 20,
                color: '#FFF',
              }}
            >
              Point2Point
            </Text>
          </Body>
          <Right style={{}} />
        </Header>
        <Content
          contentContainerStyle={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <ScrollView
            keyboardShouldPersistTaps="handled"
            style={{ padding: 10 }}
          >
            <Loader loading={this.state.isLoading} color="#11bde4" />
            <View>
              <Text style={{ marginTop: 20 }}>
                <Text style={{ fontSize: 22 }}>
                  Enter the four digit code sent to you at
                </Text>
                <Text style={{ fontSize: 22, fontWeight: 'bold' }}>
                  {' '}
                  +91 {this.state.phone_number.substr(0, 5)}{' '}
                  {this.state.phone_number.substr(5)}.
                </Text>
                {this.state.invalid_otp && (
                  <Text style={MasterStyles.notice}>
                    {' '}
                    Did you enter the correct mobile number?
                  </Text>
                )}
              </Text>
              <TextInput
                keyboardType="numeric"
                style={MasterStyles.textinput}
                onChangeText={value => this.setState({ otp: value })}
                onSubmitEditing={this.verifyOtp.bind(this)}
              />
              {this.state.invalid_otp && (
                <Text style={MasterStyles.error}>
                  The sms passcode you have entered is incorrect
                </Text>
              )}

              <TouchableOpacity
                style={MasterStyles.button}
                onPress={this.verifyOtp.bind(this)}
              >
                <Text style={MasterStyles.btntext}>Submit</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </Content>
      </Container>
    );
  }
}
