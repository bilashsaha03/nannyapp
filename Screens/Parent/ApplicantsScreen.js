import React, { Component } from 'react';
import {
  ScrollView,
  Text,
  View,
  AsyncStorage,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {
  Icon,
  Container,
  Header,
  Content,
  Left,
  Body,
  Right,
} from 'native-base';
import NavigationService from '../../NavigationService';
import MasterStyles from '../../Styles';
import Loader from 'react-native-modal-loader';
import { List, ListItem, Input } from 'react-native-elements';
import {FormInput} from "react-native-elements";

export default class ApplicantsScreen extends Component {
  constructor() {
    super();
    this.state = {
      applicants: [],
      search_key: "",
      isLoading: false,
      job_id: 0
    };
  }

  async getApplicants(jobId) {
    this.setState({ isLoading: true });
    try {
      let response = await fetch(`${NavigationService.api_url}/job_applications?job_id=${jobId}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'enableEmptySections':true,
          'Authorization': 'Token token=' + await AsyncStorage.getItem('authentication_token')
        },
      });
      let responseJson = await response.json();
      if (response.ok) {
        this.setState({ applicants: responseJson.job_applications, isLoading: false, job_id: jobId });
      } else {
        this.setState({ isLoading: false });
        alert(`Errors:\n${responseJson.error}`);
      }
    } catch (error) {
      this.setState({ isLoading: false });
      alert('Something went wrong, please try again.');
    }
  }


  componentDidMount() {
    let jobId = this.props.navigation.getParam('jobId', '0')
    this.getApplicants(jobId);
  }

  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: '#11bde4' }}>
          <Left style={{ paddingTop: 20 }}>
            <Icon
              name="ios-menu"
              style={{ color: '#FFF' }}
              onPress={() => this.props.navigation.openDrawer()}
            />
          </Left>
          <Body>
            <Text style={{ paddingTop: 20, fontWeight: 'bold', color: '#FFF' }}>
              All Applicants
            </Text>
          </Body>
          <Right style={{}} />
        </Header>
        <Content
          contentContainerStyle={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <ScrollView style={{ alignSelf: 'stretch', flex: 1, padding: 4 }}>
            <Loader loading={this.state.isLoading} color="#11bde4" />

            <List containerStyle={{ borderTopColor: '#11bde4' }}>
              <FlatList
                data={this.state.applicants}
                renderItem={({ item }) => (
                  <ListItem
                    containerStyle={{ borderBottomColor: '#11bde4' }}
                    titleStyle={{ color: '#0d0b59', paddingLeft: 16, fontSize: 17, fontWeight: 'bold'}}
                    title={`${item.user.name}`}
                    subtitle={
                      <View style={MasterStyles.subtitleView}>
                        <Text style={MasterStyles.subtitleText}>
                          {' '}
                          Bid Amount : {item.bid_amount}{' Tk'}
                        </Text>
                        <Text style={MasterStyles.subtitleText}>
                          {' '}
                          Police Station :{item.user.police_station.name}
                        </Text>



                      </View>

                    }
                    leftIcon={{ name: 'account-circle', color: 'blue' }}
                    onPress={() => {
                        this.props.navigation.navigate('jobApplicationDetail', {
                            jobApplicationId: item.id,
                        })
                    }
                    }

                  />
                )}
                keyExtractor={item => item.id}
              />
            </List>
          </ScrollView>

          <TouchableOpacity
            style={MasterStyles.button}
            onPress={() => {this.props.navigation.navigate('RunningJobs')}}>
            <Text style={MasterStyles.btntext}>Done</Text>
          </TouchableOpacity>

        </Content>
      </Container>
    );
  }
}
