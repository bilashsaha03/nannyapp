import React, { Component } from 'react';
import {
  ScrollView,
  Text,
  View,
  AsyncStorage,
  TouchableOpacity,
  FlatList,
    Image
} from 'react-native';
import {
  Icon,
  Container,
  Header,
  Content,
  Left,
  Body,
  Right,
} from 'native-base';
import NavigationService from '../../NavigationService';
import MasterStyles from '../../Styles';
import Loader from 'react-native-modal-loader';
import { List, ListItem, Input } from 'react-native-elements';
import {FormInput} from "react-native-elements";

export default class ApplicationDetailScreenScreen extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: false,
        hired: false,
        completed: false,
        job_application: {
            user: {
                police_station: {}
            },
            job: {

            }
        },
      job_application_id: 0
    };
  }

  async getJobApplication(jobApplicationId) {
      this.setState({ isLoading: true });
      try {
          let response = await fetch(`${NavigationService.api_url}/job_applications/${jobApplicationId}`, {
              method: 'GET',
              headers: {
                  'Content-Type': 'application/json',
                  'enableEmptySections':true,
                  'Authorization': 'Token token=' + await AsyncStorage.getItem('authentication_token')
              },
          });
          let responseJson = await response.json();
          //console.log(responseJson);
          if (response.ok) {
              this.setState({ job_application: responseJson.job_application, isLoading: false, job_application_id: jobApplicationId });
          } else {
              this.setState({ isLoading: false });
              alert(`Errors:\n${responseJson.error}`);
          }
      } catch (error) {
          this.setState({ isLoading: false });
          alert('Something went wrong, please try again.');
      }
  }


  async hire(applicantId) {
    this.setState({ isLoading: true });
    try {
      let response = await fetch(
        `${NavigationService.api_url}/job_applications/${this.state.job_application.id}`,
        {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
            'enableEmptySections':true,
            'Authorization': 'Token token=' + await AsyncStorage.getItem('authentication_token')
          },
          body: JSON.stringify({
            job_application: {
              status: "HIRED"
            }
          }),
        },
      );
      let responseJson = await response.json();
      if (response.ok) {
        this.setState({ isLoading: false,hired: true }, () => {
          console.log(responseJson);
        });
      } else {
        this.setState({ isLoading: false });
      }
    } catch (error) {
      console.error(error);
      this.setState({ isLoading: false });
    }
  }


  componentDidMount() {
    let jobApplicationId = this.props.navigation.getParam('jobApplicationId', '0')
    this.getJobApplication(jobApplicationId);
  }
    async complete(jobId) {
        this.setState({ isLoading: true });
        try {
            let response = await fetch(
                `${NavigationService.api_url}/jobs/${jobId}`,
                {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        'enableEmptySections':true,
                        'Authorization': 'Token token=' + await AsyncStorage.getItem('authentication_token')
                    },
                    body: JSON.stringify({
                        job: {
                            status: "COMPLETED"
                        }
                    }),
                },
            );
            let responseJson = await response.json();
            if (response.ok) {
                this.setState({ isLoading: false,completed: true }, () => {
                    //this.props.navigation.navigate('CompletedJobs')
                });
            } else {
                this.setState({ isLoading: false });
            }
        } catch (error) {
            console.error(error);
            this.setState({ isLoading: false });
        }
    }

  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: '#11bde4' }}>
          <Left style={{ paddingTop: 20 }}>
            <Icon
              name="ios-menu"
              style={{ color: '#FFF' }}
              onPress={() => this.props.navigation.openDrawer()}
            />
          </Left>
          <Body>
            <Text style={{ paddingTop: 20, fontWeight: 'bold', color: '#FFF' }}>
              Application Details
            </Text>
          </Body>
          <Right style={{}} />
        </Header>
        <Content
          contentContainerStyle={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
            <ScrollView style={{ alignSelf: 'stretch', flex: 1, padding: 10 }}>
                <Loader loading={this.state.isLoading} color="#11bde4" />
                <Text style={MasterStyles.titleTextInside}>Job Information</Text>
                <Text style={MasterStyles.titleApplicationDetails}>{this.state.job_application.job.title}</Text>
                <Text style={MasterStyles.descriptionText}>{this.state.job_application.job.description}</Text>

                <View style={{flexDirection: 'row'}}>
                    <Text style={MasterStyles.detailLabel}>Budget :</Text>
                    <Text style={MasterStyles.detailValue}>{this.state.job_application.job.budget} Tk</Text>
                </View>
                {this.state.job_application.job.job_type == 'Recurring' &&
                <View style={{flexDirection: 'row'}}>
                    <Text style={MasterStyles.detailLabel}>Weekdays :</Text>
                    <Text style={MasterStyles.detailValue}>{this.state.job_application.job.weekdays} </Text>
                </View>
                }
                <View style={{flexDirection: 'row'}}>
                    <Text style={MasterStyles.detailLabel}>Created :</Text>
                    <Text style={MasterStyles.detailValue}>{this.state.job_application.job.created_at} ago </Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <Text style={MasterStyles.detailLabel}>Skills :</Text>
                    <Text style={MasterStyles.detailValue}>{this.state.job_application.job.required_skills} </Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <Text style={MasterStyles.detailLabel}>Services :</Text>
                    <Text style={MasterStyles.detailValue}>{this.state.job_application.job.required_services} </Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <Text style={MasterStyles.detailLabel}>Age Range :</Text>
                    <Text style={MasterStyles.detailValue}>{this.state.job_application.job.age_range} </Text>
                </View>
                <Text style={MasterStyles.titleTextInsideWithBorderTop}>Nanny Information</Text>
                <View style={{flexDirection: 'row'}}>
                    <Text style={MasterStyles.detailLabel}>Bid Amount :</Text>
                    <Text style={MasterStyles.detailValue}>{this.state.job_application.bid_amount} Tk</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <Text style={MasterStyles.detailLabel}>Bid Submitted :</Text>
                    <Text style={MasterStyles.detailValue}>{this.state.job_application.created_at} ago</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <Text style={MasterStyles.detailLabel}></Text>
                    <Text style={MasterStyles.detailValue}><Image source={{ uri: this.state.job_application.user.photo_url }} style={MasterStyles.ProfilePhoto} /></Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <Text style={MasterStyles.detailLabel}>Full Name :</Text>
                    <Text style={MasterStyles.detailValue}>{this.state.job_application.user.name}</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <Text style={MasterStyles.detailLabel}>Age :</Text>
                    <Text style={MasterStyles.detailValue}>{this.state.job_application.user.age}</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <Text style={MasterStyles.detailLabel}>Skills :</Text>
                    <Text style={MasterStyles.detailValue}>{this.state.job_application.user.all_skills} {' '}</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <Text style={MasterStyles.detailLabel}>Services :</Text>
                    <Text style={MasterStyles.detailValue}>{this.state.job_application.user.all_services} {' '}</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <Text style={MasterStyles.detailLabel}>Police Station :</Text>
                    <Text style={MasterStyles.detailValue}>{this.state.job_application.user.police_station.name}</Text>
                </View>

                { this.state.job_application.status == 'HIRED' &&
                <View style={{flexDirection: 'row'}}>
                    <Text style={MasterStyles.detailLabel}>Phone :</Text>
                    <Text style={MasterStyles.detailValue}>{this.state.job_application.user.mobile_number}</Text>
                </View>
                }
                <View style={{flexDirection: 'row'}}>
                    <Text style={MasterStyles.detailLabel}>{' '}</Text>
                </View>
            </ScrollView>



            { this.state.job_application.status != 'HIRED' && !this.state.hired &&
            <TouchableOpacity
                style={MasterStyles.button}
                onPress={this.hire.bind(this, this.state.job_application.user.id)}>
                <Text style={MasterStyles.btntext}>HIRE</Text>
            </TouchableOpacity>
            }
            { this.state.job_application.status != 'HIRED' && this.state.hired &&
            <Text style={{fontSize: 15,color: "#137617"}}>
                Hired Successfully !!
            </Text>
            }
            { this.state.job_application.status == 'HIRED' && !this.state.completed &&
            <TouchableOpacity
                style={MasterStyles.completeButton}
                onPress={this.complete.bind(this, this.state.job_application.job_id)}>
                <Text style={MasterStyles.btntext}>COMPLETE</Text>
            </TouchableOpacity>
            }
            { this.state.job_application.status == 'HIRED' && this.state.completed &&
            <Text style={{fontSize: 15,color: "#137617"}}>
                Completed Successfully !!
            </Text>
            }


        </Content>
      </Container>
    );
  }
}
