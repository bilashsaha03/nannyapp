import React, {Component} from 'react';
import {
    ScrollView,
    Text,
    View,
    AsyncStorage,
    TouchableOpacity,
    SafeAreaView,
    StyleSheet,
    Picker, TextInput
} from 'react-native';
import {
    Icon,
    Container,
    Header,
    Content,
    Left,
    Body,
    Right,
} from 'native-base';
import moment from 'moment';
import NavigationService from '../../NavigationService';
import MasterStyles from '../../Styles';
import Loader from 'react-native-modal-loader';
import MaterialTabs from 'react-native-material-tabs';
import {List, ListItem} from 'react-native-elements';
import {FormLabel, FormInput, FormValidationMessage, CheckBox} from 'react-native-elements'
import DatePicker from 'react-native-datepicker'
import { ImagePicker } from 'expo';
import { Dropdown } from 'react-native-material-dropdown';
export default class CreateHostScreen extends Component {
    constructor() {
        super();
        this.state = {
            host: {
                description: '',
                availability: true,
                price: 20000,
            }

        };
    }

    async createHost() {
        console.log(this.state.host);
        this.setState({isLoading: true});
        try {
            let response = await fetch(`${NavigationService.api_url}/hosts`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'enableEmptySections': true,
                    'Authorization': 'Token token=' + await AsyncStorage.getItem('authentication_token')
                },
                body: JSON.stringify({
                host: this.state.host
            })
            });
            let responseJson = await response.json();
            if (response.ok) {
                this.setState({isLoading: false}, () => {
                    this.props.navigation.navigate('Hosts')
                });

            } else {
                this.setState({isLoading: false, warning_message: true});
            }
        } catch (error) {
            console.log(error)
            this.setState({isLoading: false});
            alert('Something went wrong, please try again.');
        }

    }

    componentDidMount() {

    }

    componentWillReceiveProps() {

    }






    render() {



        return (
            <Container>
                <Header style={{backgroundColor: '#11bde4'}}>
                    <Left style={{paddingTop: 20}}>
                        <Icon
                            name="ios-menu"
                            style={{color: '#FFF'}}
                            onPress={() => this.props.navigation.openDrawer()}
                        />
                    </Left>
                    <Body>

                    <Text
                        style={{
                            paddingTop: 20,
                            fontWeight: 'bold',
                            fontSize: 20,
                            color: '#FFF',
                        }}
                    >Create Host</Text>

                    </Body>
                    <Right style={{}}/>
                </Header>
                <Content contentContainerStyle={{flex: 1, alignSelf: 'stretch'}}>
                    <ScrollView style={MasterStyles.scrollViewStyle}>
                        <Loader loading={this.state.isLoading} color="#11bde4"/>

                        <FormLabel labelStyle={{color: "#000"}}>Description</FormLabel>
                        <View style={MasterStyles.textAreaContainer}>
                            <TextInput
                                style={MasterStyles.textArea}
                                underlineColorAndroid="transparent"
                                placeholder="Write Description Here"
                                placeholderTextColor="black"
                                value={this.state.host.description}
                                numberOfLines={10}
                                onChangeText={(text) => {
                                    let host = this.state.host
                                    host.description = text
                                    this.setState({host: host})
                                }}
                                multiline={true}
                            />
                        </View>



                        <CheckBox
                            title='Available'
                            checked={this.state.host.availability}
                            onPress={() => this.setState({availability: !this.state.availability})}
                        />

                        <FormLabel labelStyle={{color: "#000"}}>Monthly Rent (in Taka)</FormLabel>
                        <FormInput inputStyle={MasterStyles.fromInputText} keyboardType="numeric"
                                   value={this.state.host.price} onChangeText={(text) => {
                            let host = this.state.host
                            host.price = text
                            this.setState({host: host})
                        }}/>

                    </ScrollView>
                    <TouchableOpacity
                        style={MasterStyles.button}
                        onPress={this.createHost.bind(this)}>
                        <Text style={MasterStyles.btntext}>Submit</Text>
                    </TouchableOpacity>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
