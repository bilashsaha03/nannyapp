import React, {Component} from 'react';
import {
    ScrollView,
    Text,
    View,
    AsyncStorage,
    TouchableOpacity,
    SafeAreaView,
    StyleSheet,
    Picker, TextInput
} from 'react-native';
import {
    Icon,
    Container,
    Header,
    Content,
    Left,
    Body,
    Right,
} from 'native-base';
import moment from 'moment';
import NavigationService from '../../NavigationService';
import MasterStyles from '../../Styles';
import Loader from 'react-native-modal-loader';
import MaterialTabs from 'react-native-material-tabs';
import {List, ListItem} from 'react-native-elements';
import {FormLabel, FormInput, FormValidationMessage, CheckBox} from 'react-native-elements'
import DatePicker from 'react-native-datepicker'
import { ImagePicker } from 'expo';
import { Dropdown } from 'react-native-material-dropdown';
export default class CreateJobScreen extends Component {
    constructor() {
        super();
        let date = new Date();
        this.state = {
            confSkills: [],
            confServices: [],
            confAgeRanges: [],
            age_ranges: [],
            date: moment(date).format("DD/MM/YYYY HH:mm"),
            weekdays: ['Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'],
            job: {
                start_date_time: moment(date).format(),
                weekdays: [],
                skill_ids: [],
                service_ids: [],
                age_range_ids: [],
            }

        };
    }

    async createJob() {
        console.log(this.state.job);
        this.setState({isLoading: true});
        try {
            let response = await fetch(`${NavigationService.api_url}/jobs`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'enableEmptySections': true,
                    'Authorization': 'Token token=' + await AsyncStorage.getItem('authentication_token')
                },
                body: JSON.stringify({
                job: this.state.job
            })
            });
            let responseJson = await response.json();
            if (response.ok) {
                this.setState({isLoading: false}, () => {
                    this.props.navigation.navigate('PostedJobs')
                });

            } else {
                this.setState({isLoading: false, warning_message: true});
            }
        } catch (error) {
            console.log(error)
            this.setState({isLoading: false});
            alert('Something went wrong, please try again.');
        }

    }

    componentDidMount() {
        let job = this.state.job
        job.job_type = this.props.navigation.getParam('jobType', '0')
        this.setState({job: job},() => {console.log(this.state.job)})
        this.getSkillOptions();
        this.getAgeRangesOptions();
        this.getServiceOptions();
    }

    componentWillReceiveProps() {
        this.getSkillOptions();
        this.getAgeRangesOptions();
        this.getServiceOptions();
    }

    setWeekdays(weekday) {
        if (this.state.job.weekdays.includes(weekday)) {
            weekdays = this.state.job.weekdays
            weekdays = weekdays.filter((w) => {
                return w != weekday
            })
            let job = this.state.job
            job.weekdays = weekdays
            this.setState({job: job},() => {console.log(this.state.weekdays)})
        }
        else {
            weekdays = this.state.job.weekdays
            weekdays.push(weekday)
            let job = this.state.job
            job.weekdays = weekdays
            this.setState({job: job},() => {console.log(this.state.weekdays)})
        }
    }
    async getSkillOptions(){
        this.setState({ isLoading: true });
        try {
            let response = await fetch(`${NavigationService.api_url}/skills`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'enableEmptySections':true,
                    'Authorization': 'Token token=' + await AsyncStorage.getItem('authentication_token')
                },
            });
            let responseJson = await response.json();
            if (response.ok) {
                this.setState({ confSkills: responseJson.skills, isLoading: false });
            } else {
                this.setState({ isLoading: false });
                alert(`Errors:\n${responseJson.error}`);
            }
        } catch (error) {
            this.setState({ isLoading: false });
            alert('Something went wrong, please try again.');
        }

    }

    async getServiceOptions(){
        this.setState({ isLoading: true });
        try {
            let response = await fetch(`${NavigationService.api_url}/services`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'enableEmptySections':true,
                    'Authorization': 'Token token=' + await AsyncStorage.getItem('authentication_token')
                },
            });
            let responseJson = await response.json();
            if (response.ok) {
                this.setState({ confServices: responseJson.services, isLoading: false });
            } else {
                this.setState({ isLoading: false });
                alert(`Errors:\n${responseJson.error}`);
            }
        } catch (error) {
            this.setState({ isLoading: false });
            alert('Something went wrong, please try again.');
        }

    }

    async getAgeRangesOptions(){
        this.setState({ isLoading: true });
        try {
            let response = await fetch(`${NavigationService.api_url}/age_ranges`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'enableEmptySections':true,
                    'Authorization': 'Token token=' + await AsyncStorage.getItem('authentication_token')
                },
            });
            let responseJson = await response.json();
            if (response.ok) {
                this.setState({ age_ranges: responseJson.age_ranges, isLoading: false });
            } else {
                this.setState({ isLoading: false });
                alert(`Errors:\n${responseJson.error}`);
            }
        } catch (error) {
            this.setState({ isLoading: false });
            alert('Something went wrong, please try again.');
        }

    }

    setCheckbox(skill){
        if(this.state.job.skill_ids.includes(skill.value)){
            skill_ids = this.state.job.skill_ids
            skill_ids = skill_ids.filter((id) => {return id != skill.value})
            let job = this.state.job
            job.skill_ids = skill_ids
            this.setState({job: job},()=> {console.log(this.state.job)})
        }
        else{
            skill_ids = this.state.job.skill_ids
            skill_ids.push(skill.value)
            let job = this.state.job
            job.skill_ids = skill_ids
            this.setState({job: job})
        }
    }

    setServiceCheckbox(service){
        if(this.state.job.service_ids.includes(service.value)){
            service_ids = this.state.job.service_ids
            service_ids = service_ids.filter((id) => {return id != service.value})
            let job = this.state.job
            job.service_ids = service_ids
            this.setState({job: job})
        }
        else{
            service_ids = this.state.job.service_ids
            service_ids.push(service.value)
            let job = this.state.job
            job.service_ids = service_ids
            this.setState({job: job})
        }
    }




    render() {
        let skills = this.state.confSkills.map(skill => {
            return <CheckBox
                title={skill.label}
                checkedIcon='check-circle'
                uncheckedIcon='circle-o'
                iconRight
                right
                size={30}
                containerStyle={{ alignItems: 'flex-end' }}
                textStyle={{ flex:1, flexDirection: 'column', alignSelf: 'center' }}
                checked={this.state.job.skill_ids.includes(skill.value)}
                onPress={() => this.setCheckbox(skill) }
            />
        });

        let services = this.state.confServices.map(service => {
            return <CheckBox
                title={service.label}
                checkedIcon='check-circle'
                uncheckedIcon='circle-o'
                iconRight
                right
                size={30}
                containerStyle={{ alignItems: 'flex-end' }}
                textStyle={{ flex:1, flexDirection: 'column', alignSelf: 'center' }}
                checked={this.state.job.service_ids.includes(service.value)}
                onPress={() => this.setServiceCheckbox(service) }
            />
        });

        let days = this.state.weekdays.map(day => {
            return <CheckBox
                title={day}
                checkedIcon='check-circle'
                uncheckedIcon='circle-o'
                iconRight
                right
                size={30}
                containerStyle={{alignItems: 'flex-end'}}
                textStyle={{flex: 1, flexDirection: 'column', alignSelf: 'center'}}
                checked={this.state.job.weekdays.includes(day)}
                onPress={() => this.setWeekdays(day) }
            />
        });


        return (
            <Container>
                <Header style={{backgroundColor: '#11bde4'}}>
                    <Left style={{paddingTop: 20}}>
                        <Icon
                            name="ios-menu"
                            style={{color: '#FFF'}}
                            onPress={() => this.props.navigation.openDrawer()}
                        />
                    </Left>
                    <Body>
                    { this.state.job.job_type == 'ONCE' &&
                    <Text
                        style={{
                            paddingTop: 20,
                            fontWeight: 'bold',
                            fontSize: 20,
                            color: '#FFF',
                        }}
                    >Single Job (একক কাজ)</Text>
                    }
                    { this.state.job.job_type == 'RECURRING' &&
                    <Text
                        style={{
                            paddingTop: 20,
                            fontWeight: 'bold',
                            fontSize: 20,
                            color: '#FFF',
                        }}
                    >Recurring Job (পুনরাবৃত্তি কাজ)</Text>
                    }
                    </Body>
                    <Right style={{}}/>
                </Header>
                <Content contentContainerStyle={{flex: 1, alignSelf: 'stretch'}}>
                    <ScrollView style={MasterStyles.scrollViewStyle}>
                        <Loader loading={this.state.isLoading} color="#11bde4"/>

                        <FormLabel labelStyle={{color: "#000"}}>Title (কাজের নাম)</FormLabel>
                        <FormInput inputStyle={MasterStyles.fromInputText} value={this.state.job.title}
                                   onChangeText={(text) => {
                                       let job = this.state.job
                                       job.title = text
                                       this.setState({job: job})
                                   }}/>

                        <View style={MasterStyles.textAreaContainer}>
                            <TextInput
                                style={MasterStyles.textArea}
                                underlineColorAndroid="transparent"
                                placeholder="Tell us about your child (আপনার সন্তান সম্পর্কে বলুন)"
                                placeholderTextColor="black"
                                value={this.state.job.description}
                                numberOfLines={10}
                                onChangeText={(text) => {
                                    let job = this.state.job
                                    job.description = text
                                    this.setState({job: job})
                                }}
                                multiline={true}
                            />
                        </View>

                        <FormLabel labelStyle={{color: "#000"}}>Start Date and Time (কাজের দিন এবং সময়)</FormLabel>
                        <DatePicker
                            style={{width: 320}}
                            date={this.state.date}
                            mode="datetime"
                            placeholder="select date"
                            format="DD-MM-YYYY HH:mm"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            is24Hour={true}
                            customStyles={{
                                dateIcon: {
                                    position: 'absolute',
                                    left: 0,
                                    top: 4,
                                    marginLeft: 0
                                },
                                dateInput: {
                                    marginLeft: 36
                                }
                            }}
                            onDateChange={(date) => {
                                let job = this.state.job
                                job.start_date_time = moment(date, 'DD/MM/YYYY HH:mm').format()
                                this.setState({job: job, date: date})
                            }}
                        />

                        <FormLabel labelStyle={{color: "#000"}}>Estimated Cost(Tk)</FormLabel>
                        <FormInput inputStyle={MasterStyles.fromInputText} keyboardType="numeric"
                                   value={this.state.job.budget} onChangeText={(text) => {
                            let job = this.state.job
                            job.budget = text
                            this.setState({job: job})
                        }}/>

                        <FormLabel labelStyle={{color: "#000"}}>Duration (in hours)</FormLabel>
                        <FormInput inputStyle={MasterStyles.fromInputText} keyboardType="numeric"
                                   value={this.state.job.duration} onChangeText={(text) => {
                            let job = this.state.job
                            job.duration = text
                            this.setState({job: job})
                        }}/>

                        <FormLabel labelStyle={{color: "#000"}}>Skills</FormLabel>
                        {skills}

                        <FormLabel labelStyle={{color: "#000"}}>Services</FormLabel>
                        {services}

                        <FormLabel labelStyle={{color: "#000"}}>Age Range</FormLabel>

                        <Dropdown containerStyle={{marginLeft: 15, marginRight: 15}}
                                  label='Age Range'
                                  data={this.state.age_ranges}
                                  value={this.state.job.age_ranges_id}
                                  onChangeText={(text) => {
                                      let job = this.state.job
                                      job.age_range_ids.push(text)
                                      this.setState({job: job})
                                  }}

                        />

                        { this.state.job.job_type == 'RECURRING' &&

                        <View><FormLabel labelStyle={{color: "#000"}}>Repeats of week</FormLabel>
                        {days}
                            </View>
                        }

                    </ScrollView>
                    <TouchableOpacity
                        style={MasterStyles.button}
                        onPress={this.createJob.bind(this)}>
                        <Text style={MasterStyles.btntext}>Submit</Text>
                    </TouchableOpacity>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
