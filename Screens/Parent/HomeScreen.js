import React, { Component } from 'react';
import {
  ScrollView,
  Text,
  View,
  AsyncStorage,
  TouchableOpacity,
  SafeAreaView,
  StyleSheet,
  FlatList,
  Image
} from 'react-native';
import {
  Icon,
  Container,
  Header,
  Content,
  Left,
  Body,
  Right,
} from 'native-base';
import moment from 'moment';
import NavigationService from '../../NavigationService';
import MasterStyles from '../../Styles';
import Loader from 'react-native-modal-loader';
import MaterialTabs from 'react-native-material-tabs';
import { List, ListItem } from 'react-native-elements';
import { FormLabel, FormInput, FormValidationMessage, CheckBox } from 'react-native-elements'
import CustomMultiPicker from 'react-native-multiple-select-list';
import DatePicker from 'react-native-datepicker';

export default class ProfileScreen extends Component {
  constructor() {
    super();
    this.state = {
      bookings: [],
      isLoading: false,
      selectedTab: 0,
      confSkills: [],
      confServices: [],
      confAgeRanges: [],
      date_of_birth: moment(),
      name: "",
      user: {
        skill_ids: [],
        service_ids: [],
        age_range_ids: []
      }
    };
  }

  async getName() {
    this.state.name = await AsyncStorage.getItem('name')
  }

  componentDidMount() {
     this.getName()
  }

  componentWillReceiveProps() {
    this.getName()
  }

  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: '#11bde4' }}>
          <Left style={{ paddingTop: 20 }}>
            <Icon
              name="ios-menu"
              style={{ color: '#FFF' }}
              onPress={() => this.props.navigation.openDrawer()}
            />
          </Left>
          <Body>
            <Text
              style={{
                paddingTop: 20,
                fontWeight: 'bold',
                fontSize: 20,
                color: '#FFF',
              }}
            >
              Nanny App
            </Text>
          </Body>
          <Right style={{}} />
        </Header>
          <Content
            contentContainerStyle={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: '#11bde4',
            }}
          >
            <Loader loading={this.state.isLoading} color="#11bde4" />

            <Image
              style={MasterStyles.ImageStyle}
              source={require('../../assets/logo.png')}
            />
            <Text style={{ fontSize: 20, color: '#FFF', fontWeight: 'bold', alignSelf: 'center' }}>
              {`Welcome ${this.state.name}`} !
            </Text>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
