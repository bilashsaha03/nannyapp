import React, { Component } from 'react';
import {
  ScrollView,
  Text,
  View,
  AsyncStorage,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {
  Icon,
  Container,
  Header,
  Content,
  Left,
  Body,
  Right,
} from 'native-base';
import NavigationService from '../../NavigationService';
import MasterStyles from '../../Styles';
import Loader from 'react-native-modal-loader';
import { List, ListItem, Input } from 'react-native-elements';
import {FormInput} from "react-native-elements";

export default class JobDetailsScreen extends Component {
  constructor() {
    super();
    this.state = {
      job: {},
        archived: false,
      isLoading: false,
      job_id: 0
    };
  }

  async getJob(jobId) {
    this.setState({ isLoading: true });
    try {
      let response = await fetch(`${NavigationService.api_url}/jobs/${jobId}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'enableEmptySections':true,
          'Authorization': 'Token token=' + await AsyncStorage.getItem('authentication_token')
        },
      });
      let responseJson = await response.json();
      console.log(responseJson);
      if (response.ok) {
        this.setState({ job: responseJson.job, isLoading: false, job_id: jobId },()=>{console.log('kkkkkkkkkkkkkkkkkk')});
      } else {
        this.setState({ isLoading: false });
        alert(`Errors:\n${responseJson.error}`);
      }
    } catch (error) {
      this.setState({ isLoading: false });
      alert('Something went wrong, please try again.');
    }
  }

    async archive() {
        this.setState({ isLoading: true });
        try {
            let response = await fetch(
                `${NavigationService.api_url}/jobs/${this.state.job.id}`,
                {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        'enableEmptySections':true,
                        'Authorization': 'Token token=' + await AsyncStorage.getItem('authentication_token')
                    },
                    body: JSON.stringify({
                        job: {
                            status: "ARCHIVED"
                        }
                    }),
                },
            );
            let responseJson = await response.json();
            if (response.ok) {
                this.setState({ isLoading: false,archived: true }, () => {
                    console.log(responseJson);
                });
            } else {
                this.setState({ isLoading: false });
            }
        } catch (error) {
            console.error(error);
            this.setState({ isLoading: false });
        }
    }

  componentDidMount() {
    let jobId = this.props.navigation.getParam('jobId', '0')
    this.getJob(jobId);
  }

  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: '#11bde4' }}>
          <Left style={{ paddingTop: 20 }}>
            <Icon
              name="ios-menu"
              style={{ color: '#FFF' }}
              onPress={() => this.props.navigation.openDrawer()}
            />
          </Left>
          <Body>
            <Text style={{ paddingTop: 20, fontWeight: 'bold', color: '#FFF' }}>
              Job Details
            </Text>
          </Body>
          <Right style={{}} />
        </Header>
        <Content
          contentContainerStyle={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <ScrollView style={{ alignSelf: 'stretch', flex: 1, padding: 10 }}>
            <Loader loading={this.state.isLoading} color="#11bde4" />
            <Text style={MasterStyles.titleTextInside}>{this.state.job.title}</Text>
            <Text style={MasterStyles.descriptionText}>{this.state.job.description}</Text>

            <View style={{flexDirection: 'row'}}>
              <Text style={MasterStyles.detailLabel}>Created :</Text>
              <Text style={MasterStyles.detailValue}>{this.state.job.created_at} ago</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={MasterStyles.detailLabel}>Type :</Text>
              <Text style={MasterStyles.detailValue}>{this.state.job.job_type}</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={MasterStyles.detailLabel}>Starts :</Text>
              <Text style={MasterStyles.detailValue}>{this.state.job.start_date_time}</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={MasterStyles.detailLabel}>Duration :</Text>
              <Text style={MasterStyles.detailValue}>{this.state.job.duration} hours</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={MasterStyles.detailLabel}>Skills :</Text>
              <Text style={MasterStyles.detailValue}>{this.state.job.required_skills} Hours</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={MasterStyles.detailLabel}>Services :</Text>
              <Text style={MasterStyles.detailValue}>{this.state.job.required_services} Hours</Text>
            </View>
              <View style={{flexDirection: 'row'}}>
              <Text style={MasterStyles.detailLabel}>Age Range :</Text>
              <Text style={MasterStyles.detailValue}>{this.state.job.age_range}</Text>
            </View>
              { this.state.job.job_type == 'Recurring' &&
              <View style={{flexDirection: 'row'}}>
                <Text style={MasterStyles.detailLabel}>Weekdays :</Text>
                <Text style={MasterStyles.detailValue}>{this.state.job.weekdays}</Text>
              </View>
              }


          </ScrollView>
            <TouchableOpacity
                style={MasterStyles.button}
                onPress={() => {this.props.navigation.navigate('PostedJobs')}}>
                <Text style={MasterStyles.btntext}>BACK</Text>
            </TouchableOpacity>
            <Text>OR</Text>
            { !this.state.archived &&
            <TouchableOpacity
                style={MasterStyles.archiveButton}
                onPress={this.archive.bind(this)}>
              <Text style={MasterStyles.btntext}>ARCHIVE</Text>
            </TouchableOpacity>
            }
            { this.state.archived &&
            <Text style={{fontSize: 15,color: "#137617"}}>
              Archived Successfully !!
            </Text>
            }

        </Content>
      </Container>
    );
  }
}
