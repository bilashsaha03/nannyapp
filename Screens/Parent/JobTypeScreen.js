import React, { Component } from 'react';
import {
  ScrollView,
  Text,
  View,
  AsyncStorage,
  TouchableOpacity,
  SafeAreaView,
  StyleSheet,
  Picker,
    Image
} from 'react-native';
import {
  Icon,
  Container,
  Header,
  Content,
  Left,
  Body,
  Right,
} from 'native-base';
import moment from 'moment';
import NavigationService from '../../NavigationService';
import MasterStyles from '../../Styles';
import Loader from 'react-native-modal-loader';
import MaterialTabs from 'react-native-material-tabs';
import { List, ListItem } from 'react-native-elements';
import { FormLabel, FormInput, FormValidationMessage, CheckBox } from 'react-native-elements'
import DatePicker from 'react-native-datepicker';

export default class JobTypeScreen extends Component {
  constructor() {
    super();
    this.state = {
      job: {}

    };
  }

  async createJob(){
    this.setState({ isLoading: true });
    try {
      let response = await fetch(`${NavigationService.api_url}/jobs`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'enableEmptySections':true,
          'Authorization': 'Token token=' + await AsyncStorage.getItem('authentication_token')
        },
        body: JSON.stringify(this.state.job),
      });
      let responseJson = await response.json();
      if (response.ok) {
        this.setState({ isLoading: false }, () => {
           this.props.navigation.navigate('PostedJobs')
        });

      } else {
        this.setState({ isLoading: false, warning_message: true });
      }
    } catch (error) {
      console.log(error)
      this.setState({ isLoading: false });
      alert('Something went wrong, please try again.');
    }

  }

  componentDidMount() {

  }


  render() {
    let types = ["ONCE","RECURRING"].map((s) => {
      return <Picker.Item key={s} value={s} label={s} />;
    });


      return (
          <Container>
            <Header style={{ backgroundColor: '#11bde4' }}>
              <Left style={{ paddingTop: 20 }}>
                <Icon
                    name="ios-menu"
                    style={{ color: '#FFF' }}
                    onPress={() => this.props.navigation.openDrawer()}
                />
              </Left>
              <Body>
              <Text
                  style={{
                      paddingTop: 20,
                      fontWeight: 'bold',
                      fontSize: 20,
                      color: '#FFF',
                  }}
              >
                Create Job
              </Text>
              </Body>
              <Right style={{}} />
            </Header>
            <Content contentContainerStyle={{ flex: 1, alignSelf: 'stretch' }}>
              <ScrollView style={MasterStyles.scrollViewStyle}>
                <Loader loading={this.state.isLoading} color="#11bde4" />
                <View style={MasterStyles.jobTypeRow}>
                  <TouchableOpacity
                      style={MasterStyles.JobType}
                      onPress={() => {this.props.navigation.navigate('NewJob', {
                          jobType: 'ONCE',
                      })}}>
                    <Image
                        style={{width: 38, height: 37, alignSelf: 'center', marginBottom: 2}}
                        source={require('../../assets/single_job_icon.jpg')}
                    />
                    <Text style={MasterStyles.jobTitle}>Single Job (একক কাজ)</Text>
                    <Text style={MasterStyles.jobDescription}>Post a Job for a single date and time. Perfect if you only need someone every now and then</Text>
                  </TouchableOpacity>
                </View>
                <View>
                  <TouchableOpacity
                      style={MasterStyles.JobType}
                      onPress={() => {this.props.navigation.navigate('NewJob', {
                          jobType: 'RECURRING',
                      })}}>
                    <Image
                        style={{width: 36, height: 36, alignSelf: 'center', marginBottom: 2}}
                        source={require('../../assets/recurring_job_icon.jpg')}
                    />
                    <Text style={MasterStyles.jobTitle}>Recurring Job</Text>
                    <Text style={MasterStyles.jobDescription}>Post a Job for multiple weekdays. Perfect if you only need someone regularly</Text>
                  </TouchableOpacity>
                </View>
              </ScrollView>
            </Content>
          </Container>
      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
