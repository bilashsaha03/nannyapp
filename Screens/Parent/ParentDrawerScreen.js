import React, {Component} from 'react';
import {AsyncStorage, ScrollView, Text, StyleSheet} from 'react-native';
import {
    createDrawerNavigator,
    DrawerItems,
    SafeAreaView,
} from 'react-navigation';
import NavigationService from '../../NavigationService';
import LogoutScreen from '../LogoutScreen';

import {ListItem} from 'react-native-elements';
import HomeScreen from "./HomeScreen";
import JobTypeScreen from "./JobTypeScreen";
import CreateJobScreen from "./CreateJobScreen";
import CompletedJobsScreen from "./CompletedJobsScreen";
import RunningJobsScreen from "./RunningJobsScreen";
import ParentHostsScreen from "./ParentHostsScreen";
import CreateHostScreen from "./CreateHostScreen";
import PostedJobsScreen from "./PostedJobsScreen";
import JobDetailsScreen from "./JobDetailsScreen";
import ApplicantsScreen from "./ApplicantsScreen";
import ApplicationDetailScreen from "./ApplicationDetailScreen";
import ParentProfileScreen from "./ParentProfileScreen";
import {
    Icon
} from 'native-base';
class Hidden extends React.Component {
    render() {
        return null;
    }
}

export default class ParentDrawerScreen extends Component {
    constructor() {
        super();
        this.state = {
            name: '...',
            photo_url: '',
        };
    }

    async getName() {
        AsyncStorage.getItem('name').then(value => {
            this.setState({name: value});
        });
        AsyncStorage.getItem('photo_url').then(value => {
            this.setState({photo_url: value});
        });
    }

    componentDidMount() {
        this.getName();
    }

    componentWillReceiveProps() {
        this.getName();
    }

    render() {
        return (
            <ParentStart
                ref={navigatorRef => {
                    NavigationService.setParentLevelNavigator(navigatorRef);
                }}
                screenProps={{
                    saveToStore: this.saveToStore,
                    name: this.state.name,
                    phone_number: this.state.phone_number,
                    photo_url: this.state.photo_url,
                }}
            />
        );
    }
}

const CustomDrawerContentComponent = props => (
    <ScrollView>
        <SafeAreaView
            style={styles.container}
            forceInset={{top: 'always', horizontal: 'never'}}
        >
            <ListItem
                roundAvatar
                avatar={{uri: props.screenProps.photo_url}}
                key={props.screenProps.name}
                title={props.screenProps.name}
                hideChevron={true}
            />
            <DrawerItems {...props} />
            <Text style={{marginLeft: 15}}>Version: 0.1.0</Text>
        </SafeAreaView>
    </ScrollView>
);

const ParentStart = createDrawerNavigator(
    {
        Home: {
            screen: props => <HomeScreen {...props} />,
            navigationOptions: {
                title: 'Home',
            },
        },
        ParentProfile: {
            screen: props => <ParentProfileScreen {...props} />,
            navigationOptions: {
                title: 'Update Profile',
                drawerIcon: () => (
                    <Icon name='create' style={{color: '#000000'}}/>
                )
            },
        },
        CreateJob: {
            screen: props => <JobTypeScreen {...props} />,
            navigationOptions: {
                title: 'Create Job',
                drawerIcon: () => (
                    <Icon name='md-list' style={{color: '#000000'}}/>
                )

            },
        },
        NewJob: {
            screen: props => <CreateJobScreen {...props} />,
            navigationOptions: {
                title: 'JobTypes',
                drawerLabel: <Hidden />
            },
        },
        JobDetails: {
            screen: props => <JobDetailsScreen {...props} />,
            navigationOptions: {
                title: 'JobDetails',
                drawerLabel: <Hidden />
            },
        },
        PostedJobs: {
            screen: props => <PostedJobsScreen {...props} />,
            navigationOptions: {
                title: 'Posted Jobs',
                drawerIcon: () => (
                    <Icon name='bookmark' style={{color: '#800040'}}/>
                )
            },
        },
        RunningJobs: {
            screen: props => <RunningJobsScreen {...props} />,
            navigationOptions: {
                title: 'Running Jobs',
                drawerIcon: () => (
                    <Icon name='bookmark' style={{color: '#004c00'}}/>
                )
            },
        },
        CompletedJobs: {
            screen: props => <CompletedJobsScreen {...props} />,
            navigationOptions: {
                title: 'Completed Jobs',
                drawerIcon: () => (
                    <Icon name='bookmark' style={{color: '#000000'}}/>
                )
            },
        },

        Applicants: {
            screen: props => <ApplicantsScreen {...props} />,
            navigationOptions: {
                title: 'Applications',
                drawerLabel: <Hidden />
            },
        },
        jobApplicationDetail: {
            screen: props => <ApplicationDetailScreen {...props} />,
            navigationOptions: {
                title: 'Application Details',
                drawerLabel: <Hidden />
            },
        },
        Hosts: {
            screen: props => <ParentHostsScreen {...props} />,
            navigationOptions: {
                title: 'Hosts',
                drawerIcon: () => (
                    <Icon name='link' style={{color: '#004c00'}}/>
                )
            },
        },
        CreateHost: {
            screen: props => <CreateHostScreen {...props} />,
            navigationOptions: {
                title: 'Create Host',
                drawerLabel: <Hidden />
            },
        },
        Logout: {
            screen: props => <LogoutScreen {...props} />,
            navigationOptions: {
                title: 'Logout',
                drawerIcon: () => (
                    <Icon name='log-out'
                          style={{color: '#990000'}}/>
                )
            },
        }
    },
    {
        contentComponent: props => <CustomDrawerContentComponent {...props} />,
        drawerPosition: 'left',
        initialRouteName: 'ParentProfile',
        navigationOptions: {
            headerStyle: {
                backgroundColor: '#11bde4',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
                color: '#FFFFFF',
            },
        },
    },
);

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
