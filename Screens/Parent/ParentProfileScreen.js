import React, { Component } from 'react';
import {
  ScrollView,
  Text,
  View,
  AsyncStorage,
  TouchableOpacity,
  SafeAreaView,
  StyleSheet,
  FlatList,
    Button,Image, TextInput
} from 'react-native';
import {
  Icon,
  Container,
  Header,
  Content,
  Left,
  Body,
  Right,
} from 'native-base';
import moment from 'moment';
import NavigationService from '../../NavigationService';
import MasterStyles from '../../Styles';
import Loader from 'react-native-modal-loader';
import MaterialTabs from 'react-native-material-tabs';
import { List, ListItem } from 'react-native-elements';
import { FormLabel, FormInput, FormValidationMessage, CheckBox } from 'react-native-elements'
import CustomMultiPicker from 'react-native-multiple-select-list';
import DatePicker from 'react-native-datepicker';
import { ImagePicker } from 'expo';
import { Dropdown } from 'react-native-material-dropdown';
export default class ParentProfileScreen extends Component {
  constructor() {
    super();
    this.state = {
      bookings: [],
      isLoading: false,
      selectedTab: 0,
      confSkills: [],
      confServices: [],
      confAgeRanges: [],
      date_of_birth: moment(),
        image: '',
        base64_image: '',
      user: {
        skill_ids: [],
        service_ids: [],
        age_range_ids: [],
        first_name: "",
        last_name: "",
        password: "",
        email: "",
        police_stations: [],
        terms_agreed: true,
        address_line1: '',
        mobile_number: '',
        user_intro: '',
        nid: '',
        police_station_id: '',
      }
    };
  }


  async getUser() {
    this.setState({ isLoading: true });
    try {
      let response = await fetch(`${NavigationService.api_url}/user/get`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'enableEmptySections':true,
          'Authorization': 'Token token=' + await AsyncStorage.getItem('authentication_token')
        },
      });
      let responseJson = await response.json();
      if (response.ok) {
        dob = responseJson.user.date_of_birth ? moment(responseJson.user.date_of_birth) : moment("01/01/1980")
        this.setState({ user: responseJson.user,
            isLoading: false,
            image: responseJson.user.photo_url,
            date_of_birth: dob },() => {
          console.log(this.state.user.skill_ids)
        });
      } else {
        this.setState({ isLoading: false });
        alert(`Errors:\n${responseJson.error}`);
      }
    } catch (error) {
      this.setState({ isLoading: false });
      alert('Something went wrong, please try again.');
    }
  }

  async updateUser() {
    this.setState({ isLoading: true });
    try {
      let user_id = await AsyncStorage.getItem('user_id')
      let response = await fetch(
        `${NavigationService.api_url}/users/${user_id}`,
        {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
            'enableEmptySections':true,
            'Authorization': 'Token token=' + await AsyncStorage.getItem('authentication_token')
          },
          body: JSON.stringify({
            user: this.state.user,
              base64_image: this.state.base64_image
          })
        },
      );
      let responseJson = await response.json();
      if (response.ok) {
        this.setState({ user: responseJson.user, isLoading: false }, () => {
          this.props.navigation.navigate("Home")
        });
      } else {
        this.setState({ isLoading: false });
        alert(`Errors:\n${responseJson.error}`);
      }
    } catch (error) {
      this.setState({ isLoading: false });
      alert('Something went wrong, please try again.');
    }
  }

    _pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,
            aspect: [4, 3],
            base64: true
        });

        console.log(result.base64);

        if (!result.cancelled) {
            this.setState({ image: result.uri,base64_image:result.base64 },() => {
                //console.log(this.state.image)
            });

        }
    }

  componentDidMount() {
    this.getUser();
      this.getPoliceStations();
  }
    componentWillReceiveProps() {
        this.getUser();
        this.getPoliceStations();
    }

    async getPoliceStations() {
        this.setState({ isLoading: true });
        try {
            let response = await fetch(`${NavigationService.api_url}/police_stations`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'enableEmptySections':true,
                    'Authorization': 'Token token=' + await AsyncStorage.getItem('authentication_token')
                },
            });
            let responseJson = await response.json();
            if (response.ok) {
                this.setState({ police_stations: responseJson.police_stations, isLoading: false }, () => {
                    console.log("****************************")
                    console.log(this.state.police_stations)
                    console.log("****************************")
                    console.log(this.state.police_stations)
                    console.log("****************************")
                });
            } else {
                this.setState({ isLoading: false });
                alert(`Errors:\n${responseJson.error}`);
            }
        } catch (error) {
            this.setState({ isLoading: false });
            alert('Something went wrong, please try again.');
        }
    }


  render() {
      let { image } = this.state;
    return (
      <Container>
        <Header style={{ backgroundColor: '#11bde4' }}>
          <Left style={{ paddingTop: 20 }}>
            <Icon
              name="ios-menu"
              style={{ color: '#FFF' }}
              onPress={() => this.props.navigation.openDrawer()}
            />
          </Left>
          <Body>
            <Text
              style={{
                paddingTop: 20,
                fontWeight: 'bold',
                fontSize: 20,
                color: '#FFF',
              }}
            >
              Profile
            </Text>
          </Body>
          <Right style={{}} />
        </Header>
        <Content contentContainerStyle={{ flex: 1, alignSelf: 'stretch' }}>
          <ScrollView style={MasterStyles.scrollViewStyle}>
            <Loader loading={this.state.isLoading} color="#11bde4" />

            <FormLabel labelStyle={{color: "#000"}}>First Name (প্রথম নাম)</FormLabel>
            <FormInput inputStyle={MasterStyles.fromInputText} value={this.state.user.first_name} onChangeText={(text) => {
              let user = this.state.user
              user.first_name = text
              this.setState({user: user})
            }}/>

            <FormLabel labelStyle={{color: "#000"}}>Last Name (শেষ নাম)</FormLabel>
            <FormInput inputStyle={MasterStyles.fromInputText} value={this.state.user.last_name} onChangeText={(text) => {
              let user = this.state.user
              user.last_name = text
              this.setState({user: user})
            }}/>

            <FormLabel labelStyle={{color: "#000"}}></FormLabel>
            <View style={MasterStyles.textAreaContainer} >
              <TextInput
                  style={MasterStyles.textArea}
                  underlineColorAndroid="transparent"
                  placeholder="Tell something about you (নিজের সম্পর্কে কিছু বলুন)"
                  placeholderTextColor="black"
                  value={this.state.user.user_intro}
                  numberOfLines={10}
                  onChangeText={(text) => {
                      let user = this.state.user
                      user.user_intro = text
                      this.setState({user: user})
                  }}
                  multiline={true}
              />
            </View>

            <FormLabel labelStyle={{color: "#000"}}>Location (স্থান)</FormLabel>
            <FormInput inputStyle={MasterStyles.fromInputText}
                       value={this.state.user.address_line1} onChangeText={(text) => {
                let user = this.state.user
                user.address_line1 = text
                this.setState({user: user})
            }}/>

            <FormLabel labelStyle={{color: "#000"}}>Phone Number (ফোন নাম্বার)</FormLabel>
            <FormInput inputStyle={MasterStyles.fromInputText}
                       keyboardType="phone-pad"
                       value={this.state.user.mobile_number}
                       onChangeText={(text) => {
                      let user = this.state.user
                      user.mobile_number = text
                      this.setState({user: user})
                      }}
            />

            <FormLabel labelStyle={{color: "#000"}}>NID (এন আইডি নাম্বার)</FormLabel>
            <FormInput inputStyle={MasterStyles.fromInputText}
                       keyboardType="phone-pad"
                       value={this.state.user.nid} onChangeText={(text) => {
                let user = this.state.user
                user.nid = text
                this.setState({user: user})
            }}/>

            <Dropdown containerStyle={{marginLeft: 15, marginRight: 15}}
                      label='Nearest Police Station (কাছাকাছি পুলিশ স্টেশন)'
                      data={this.state.police_stations}
                      value={this.state.user.police_station_id}
                      onChangeText={(text) => {
                          let user = this.state.user
                          user.police_station_id = text
                          this.setState({user: user})
                      }}

            />


            <FormLabel labelStyle={{color: "#000"}}>Date Of Birth</FormLabel>
            <DatePicker
                style={{ width: 320 }}
                date={this.state.date_of_birth}
                mode="date"
                placeholder="select date"
                format="DD/MM/YYYY"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                androidMode="spinner"
                is24Hour={true}
                customStyles={{
                    dateIcon: {
                        position: 'absolute',
                        left: 0,
                        top: 4,
                        marginLeft: 0,
                    },
                    dateInput: {
                        marginLeft: 36,
                    },
                }}
                onDateChange={date => {
                    let user = this.state.user
                    user.date_of_birth= moment(date, 'DD/MM/YYYY').format()
                    this.setState({user: user, date_of_birth:date})
                }}
            />

            <View style={{ flex: 1, alignItems: 'center', marginTop: 10, justifyContent: 'center' }}>
              <Button
                  title="Set Profile Photo"
                  onPress={this._pickImage}
              />


                {this.state.image ?
                    <Image source={{ uri: image }} style={MasterStyles.UploadedPhoto} /> :
                    <Image
                        style={MasterStyles.UploadedPhoto}
                        source={require('../../assets/no-image.png')}
                    />}


            </View>


          </ScrollView>
          <TouchableOpacity
            style={MasterStyles.button}
            onPress={this.updateUser.bind(this)}>
            <Text style={MasterStyles.btntext}>Submit</Text>
          </TouchableOpacity>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
