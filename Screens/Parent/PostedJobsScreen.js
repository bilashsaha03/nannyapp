import React, { Component } from 'react';
import {
  ScrollView,
  Text,
  View,
  AsyncStorage,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {
  Icon,
  Container,
  Header,
  Content,
  Left,
  Body,
  Right,
} from 'native-base';
import NavigationService from '../../NavigationService';
import MasterStyles from '../../Styles';
import Loader from 'react-native-modal-loader';
import { List, ListItem, Input } from 'react-native-elements';
import {FormInput} from "react-native-elements";

export default class PostedJobsScreen extends Component {
  constructor() {
    super();
    this.state = {
      jobs: [],
      search_key: "",
      isLoading: false,
    };
  }

  async getAllJobs() {
    this.setState({ isLoading: true });
    try {
      let response = await fetch(`${NavigationService.api_url}/job/openings/`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'enableEmptySections':true,
          'Authorization': 'Token token=' + await AsyncStorage.getItem('authentication_token')
        },
      });
      let responseJson = await response.json();
      if (response.ok) {
        this.setState({ jobs: responseJson.jobs, isLoading: false });
      } else {
        this.setState({ isLoading: false });
        alert(`Errors:\n${responseJson.error}`);
      }
    } catch (error) {
      this.setState({ isLoading: false });
      alert('Something went wrong, please try again.');
    }
  }

  componentDidMount() {
    this.getAllJobs();
  }

  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: '#11bde4' }}>
          <Left style={{ paddingTop: 20 }}>
            <Icon
              name="ios-menu"
              style={{ color: '#FFF' }}
              onPress={() => this.props.navigation.openDrawer()}
            />
          </Left>
          <Body>
            <Text style={{ paddingTop: 20, fontWeight: 'bold', color: '#FFF' }}>
              Posted Jobs
            </Text>
          </Body>
          <Right style={{}} />
        </Header>
        <Content
          contentContainerStyle={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <ScrollView style={{ alignSelf: 'stretch', flex: 1, padding: 4 }}>
            <Loader loading={this.state.isLoading} color="#11bde4" />

            <List containerStyle={{ borderTopColor: '#11bde4' }}>
              <FlatList
                data={this.state.jobs}
                renderItem={({ item }) => (
                  <ListItem
                    containerStyle={{ borderBottomColor: '#11bde4' }}
                    titleStyle={{ color: '#0d0b59', paddingLeft: 16, fontSize: 17, fontWeight: 'bold'}}
                    title={`${item.title}`}
                    subtitle={
                      <View style={MasterStyles.subtitleView}>
                        <Text style={MasterStyles.subtitleText}>
                          {' '}
                          Type : {item.job_type}
                        </Text>
                        <Text style={MasterStyles.subtitleText}>
                          {' '}
                          Budget :{' '}
                          {item.budget}{' Tk'}
                        </Text>

                        <Text style={MasterStyles.subtitleText}>
                          {' '}
                          Applications :{' '}
                          {item.job_application_count}{' '}
                        </Text>
                      </View>
                    }
                    leftIcon={{ name: 'assignment', color: 'blue' }}
                    onPress={() => {
                      this.props.navigation.navigate(item.job_application_count > 0 ? 'Applicants' : 'JobDetails', {
                        jobId: item.id,
                      })
                    }
                    }
                  />
                )}
                keyExtractor={item => item.name}
              />
            </List>
          </ScrollView>
        </Content>
      </Container>
    );
  }
}
