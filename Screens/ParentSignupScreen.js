import React, {Component} from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    AsyncStorage, Picker, ScrollView, TextInput
} from 'react-native';
import {
    Icon,
    Container,
    Header,
    Content,
    Left,
    Body,
    Right,
} from 'native-base';
import NavigationService from '../NavigationService';
import Loader from 'react-native-modal-loader';
import { Dropdown } from 'react-native-material-dropdown';
import {FormLabel, FormInput, CheckBox} from 'react-native-elements'
import MasterStyles from '../Styles';

export default class SignupScreen extends Component {
    constructor() {
        super();
        this.state = {
            first_name: "",
            last_name: "",
            password: "",
            email: "",
            role: "",
            police_stations: [],
            terms_agreed: true,
            address_line1: '',
            mobile_number: '',
            user_intro: '',
            nid: '',
            police_station_id: '',
        };
    }

    componentDidMount() {
        this.getPoliceStations();

    }
    async getPoliceStations() {
        this.setState({ isLoading: true });
        try {
            let response = await fetch(`${NavigationService.api_url}/police_stations`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'enableEmptySections':true,
                    'Authorization': 'Token token=' + await AsyncStorage.getItem('authentication_token')
                },
            });
            let responseJson = await response.json();
            if (response.ok) {
                this.setState({ police_stations: responseJson.police_stations, isLoading: false }, () => {
                    console.log("****************************")
                    console.log(this.state.police_stations)
                    console.log("****************************")
                    console.log(this.state.police_stations)
                    console.log("****************************")
                });
            } else {
                this.setState({ isLoading: false });
                alert(`Errors:\n${responseJson.error}`);
            }
        } catch (error) {
            this.setState({ isLoading: false });
            alert('Something went wrong, please try again.');
        }
    }


    async signup() {
        this.setState({isLoading: true});
        try {
            let response = await fetch(`${NavigationService.api_url}/signup/`, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                        email: this.state.email,
                        password: this.state.password,
                        first_name: this.state.first_name,
                        last_name: this.state.last_name,
                        address_line1: this.state.address_line1,
                        mobile_number: this.state.mobile_number,
                        nid: this.state.nid,
                    user_intro: this.state.user_intro,
                        police_station_id: this.state.police_station_id,
                        role: 1,
                        terms_agreed: this.state.terms_agreed

                    }
                ),
            });
            let responseJson = await response.json();
            if (response.ok) {
                this.setState({isLoading: false, warning_message: false}, () => {
                    this.props.screenProps.saveToStore(
                        'authentication_token',
                        responseJson.user.auth_token,
                    );
                    this.props.screenProps.saveToStore('email', responseJson.user.email);
                    this.props.screenProps.saveToStore('user_id', `${responseJson.user.id}`);
                    this.props.screenProps.saveToStore('role', `${responseJson.role}`);
                    this.props.screenProps.saveToStore('name', `${responseJson.name}`);
                    console.log(responseJson.role)
                    if (responseJson.role == 'nanny') {
                        NavigationService.navigate_root('NannyDrawer');
                    } else if (responseJson.role == 'parent') {
                        NavigationService.navigate_root('ParentDrawer');
                    }
                });

            } else {
                this.setState({isLoading: false, warning_message: true});
            }
        } catch (error) {
            this.setState({isLoading: false});
            alert('Something went wrong, please try again.');
        }
    }

    render() {

        return (
            <Container>
                <Header style={{backgroundColor: '#11bde4'}}>
                    <Body>
                    <Text
                        style={{
                            paddingTop: 20,
                            fontWeight: 'bold',
                            fontSize: 20,
                            color: '#FFF',
                        }}
                    >
                        Parent Sign Up
                    </Text>
                    </Body>
                    <Right style={{}}/>
                </Header>
                <Content contentContainerStyle={{
                    flex: 1,
                    alignItems: 'stretch',
                    justifyContent: 'center',
                    backgroundColor: '#FFF',
                }}>
                    <ScrollView style={MasterStyles.scrollViewStyle}>

                        <Loader loading={this.state.isLoading} color="#11bde4"/>

                        <FormLabel labelStyle={{color: "#000"}}>First Name (প্রথম নাম)</FormLabel>
                        <FormInput inputStyle={MasterStyles.fromInputText}
                                   onChangeText={(text) => this.setState({first_name: text})}/>

                        <FormLabel labelStyle={{color: "#000"}}>Last Name (শেষ নাম)</FormLabel>
                        <FormInput inputStyle={MasterStyles.fromInputText}
                                   onChangeText={(text) => this.setState({last_name: text})}/>

                        <FormLabel labelStyle={{color: "#000"}}>Email (ই-মেইল)</FormLabel>
                        <FormInput inputStyle={MasterStyles.fromInputText}
                                   onChangeText={(text) => this.setState({email: text})}/>

                        <FormLabel labelStyle={{color: "#000"}}>Password (পাসওয়ার্ড)</FormLabel>
                        <FormInput inputStyle={MasterStyles.fromInputText} secureTextEntry={true}
                                   onChangeText={(text) => this.setState({password: text})}/>


                        <FormLabel labelStyle={{color: "#000"}}></FormLabel>
                        <View style={MasterStyles.textAreaContainer} >
                            <TextInput
                                style={MasterStyles.textArea}
                                underlineColorAndroid="transparent"
                                placeholder="Tell something about you (নিজের সম্পর্কে কিছু বলুন)"
                                placeholderTextColor="grey"
                                numberOfLines={10}
                                onChangeText={(text) => this.setState({user_intro: text})}
                                multiline={true}
                            />
                        </View>

                        <FormLabel labelStyle={{color: "#000"}}>Location (স্থান)</FormLabel>
                        <FormInput inputStyle={MasterStyles.fromInputText}
                                   onChangeText={(text) => this.setState({address_line1: text})}/>

                        <FormLabel labelStyle={{color: "#000"}}>Phone Number (ফোন নাম্বার)</FormLabel>
                        <FormInput keyboardType="phone-pad" inputStyle={MasterStyles.fromInputText}
                                   onChangeText={(text) => this.setState({mobile_number: text})}/>

                        <FormLabel labelStyle={{color: "#000"}}>NID (এন আইডি নাম্বার)</FormLabel>
                        <FormInput keyboardType="phone-pad" inputStyle={MasterStyles.fromInputText}
                                   onChangeText={(text) => this.setState({nid: text})}/>

                        <Dropdown containerStyle={{marginLeft: 15, marginRight: 15}}
                            label='Nearest Police Station (কাছাকাছি পুলিশ স্টেশন)'
                            data={this.state.police_stations}
                                  value={this.state.police_station}
                                  onChangeText={(text) => this.setState({police_station_id: text})}

                        />

                        <CheckBox
                            title='I am providing all correct information (আমি সব সঠিক তথ্য প্রদান করছি) '
                            checked={this.state.terms_agreed}
                            onPress={() => this.setState({terms_agreed: !this.state.terms_agreed})}
                        />

                        {this.state.warning_message && (
                            <View style={{flex: 1, alignItems: 'center'}}>
                                <Text
                                    style={{
                                        fontSize: 18,
                                        fontWeight: 'bold',
                                        color: '#F00',
                                        marginTop: 8,
                                    }}
                                >
                                    Something bad Happened !!
                                </Text>
                                <Text
                                    style={{
                                        fontSize: 18,
                                        fontWeight: 'bold',
                                        color: '#F00',
                                        marginTop: 8,
                                    }}
                                >
                                    Please fill all the above fields.
                                </Text>
                            </View>
                        )}
                    </ScrollView>

                    <TouchableOpacity
                        style={MasterStyles.button}
                        onPress={this.signup.bind(this)}>
                        <Text style={MasterStyles.btntext}>Signup</Text>
                    </TouchableOpacity>

                </Content>
            </Container>
        );
    }
}
