import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    labelText: {
        padding: 4,
        margin: 4,
        alignSelf: 'stretch',
        flex: 1,
        color: '#000000',
        fontSize: 18,
    },
    scrollViewStyle: {
        padding: 4,
        alignSelf: 'stretch',
        flex: 1,
    },
    textinput: {
        alignSelf: 'stretch',
        height: 40,
        marginBottom: 10,
        color: '#000',
        borderBottomColor: '#f8f8f8',
        borderBottomWidth: 1,
        fontSize: 26,
        width: 320,
    },
    button: {
        alignSelf: 'stretch',
        alignItems: 'center',
        padding: 20,
        marginTop: 8,
        backgroundColor: '#11bde4',
    },
    completeButton: {
        alignSelf: 'stretch',
        alignItems: 'center',
        padding: 20,
        marginTop: 8,
        backgroundColor: '#003d00',
    },
    archiveButton: {
        alignSelf: 'stretch',
        alignItems: 'center',
        padding: 20,
        marginTop: 8,
        backgroundColor: '#F80001',
    },
    createNew: {
        alignSelf: 'stretch',
        alignItems: 'center',
        padding: 20,
        backgroundColor: '#003d00',
    },
    btntext: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 20,
    },
    error: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#ff0000',
    },
    notice: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#f3b13e',
    },
    picker: {
        marginTop: 5,
        marginLeft: 10,
        backgroundColor: '#dbe1dd',
    },

    ImageStyle: {
        height: 33,
        width: 100,
        marginTop: 0,
        resizeMode: 'stretch',
        alignItems: 'center',
    },
    UploadedPhoto: {
        height: 120,
        width: 160,
        marginTop: 20,
        marginBottom: 10,
        resizeMode: 'stretch',
        alignItems: 'center',
    },
    ProfilePhoto: {
        height: 240,
        width: 320,
        marginTop: 20,
        marginBottom: 10,
        resizeMode: 'stretch',
        alignItems: 'center',
    },
    ClickToChoose: {
        marginTop: 10,
        marginBottom: 5
    },
    phone_input: {
        alignSelf: 'stretch',
        color: '#FFF',
        paddingBottom: 5,
        width: 250,
        fontSize: 26,
        paddingLeft: 3,
        paddingRight: 3,
    },
    subtitleView: {
        paddingLeft: 10,
        paddingTop: 5,
    },
    ratingImage: {
        height: 19.21,
        width: 100,
    },
    subtitleText: {
        paddingLeft: 1,
        color: '#0d0b59',
        fontSize: 15
    },
    subtitleTextDescription: {
        paddingLeft: 8,
        color: '#0d0b59',
        fontSize: 15,
        /*borderLeftColor: '#f00',
        borderLeftWidth: 3,*/

    },
    detailLabel: {
        paddingLeft: 1,
        color: '#0d0b59',
        marginTop: 3,
        fontSize: 15,
        width: 120,
        fontWeight: 'bold'
    },
    detailValue: {
        paddingLeft: 1,
        marginTop: 3,
        color: '#0d0b59',
        fontSize: 15,
        marginRight: 20,
        fontFamily: "",
        /*  borderRightColor: '#f00',
         borderRightWidth: 2,*/
        alignSelf: 'stretch',
        textAlign: 'justify'
    },
    titleText: {
        color: '#0d0b59',
        textAlign: 'justify',
        fontSize: 17,
        fontWeight: 'bold',
        flex: 1,
    },
    titleTextInside: {
        color: '#0d0b59',
        textAlign: 'justify',
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: 8,
        flex: 1,
        marginBottom: 5
    },
    titleTextInsideWithBorderTop: {
        color: '#0d0b59',
        textAlign: 'justify',
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: 10,
        paddingTop: 10,
        flex: 1,
        marginBottom: 5,
        borderTopColor: '#333',
        borderTopWidth: 1
    },
    titleApplicationDetails: {
        color: '#0d0b59',
        textAlign: 'justify',
        fontSize: 16,
        fontWeight: 'bold',
        marginTop: 2,
        flex: 1,
        marginBottom: 5
    },
    descriptionText: {
        textAlign: 'justify',
        color: '#0d0b59',
        fontSize: 16,
        flex: 1,
        marginBottom: 6
    },
    fromInputText: {
        color: "#000"
    },
    textAreaContainer: {
        borderColor: '#333',
        marginLeft: 15,
        marginRight: 15,
        borderWidth: 1,
        padding: 5,
        color: "#333"
    },
    textArea: {
        height: 150,
        color: "#333",
        justifyContent: "flex-start"
    },
    jobTypeRow: {
        backgroundColor: '#fff',
        textAlign: 'center',
        marginTop: 10,
        marginBottom: 20
    },
    JobType: {
        borderColor: '#11bde4',
        borderWidth: 1,
        padding: 8,
        margin: 8
    },
    jobTitle: {
        textAlign: 'center',
        alignSelf: 'center',
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 5
    },
    jobDescription: {
        textAlign: 'center',
        alignSelf: 'center',
        fontSize: 15,
    }
});
